from django.urls import path, include

# from users.views import login, registration, profile, logout
# from .views import SignUp, profile_settings, logout

from .views import UserAuthViewSet, EmailVerificationView, SetNewPasswordAPIView, ProfileView, PasswordChangeView, index
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView, TokenBlacklistView,
)
app_name = 'users'
router = DefaultRouter()
router.register('auth', UserAuthViewSet, basename="auth")  # роутеры для логина/ регистрации / логаута
router.register('profile', ProfileView, basename="profile")  # роутеры для просмотра и редактирования профиля
# print(router.urls)
urlpatterns = [
    path(r'users/verify_email/', EmailVerificationView.as_view(), name="resend-email"),  # роутер для запроса подтверждения почты
    path(r'users/verify_email/<str:email>/', EmailVerificationView.as_view(), name='email-verification'), # роутер для post- подтверждения почты с кодом
    path('users/auth/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'), # запрос токена (напрямую)
    path('users/auth/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'), # запрос на обновление токена
    path('users/auth/token/blacklist/', TokenBlacklistView.as_view(), name='token_blacklist'), # добавление токена в blacklist
    path(r'users/', include(router.urls)),
    path(r'users/profile/verify_old_password/', PasswordChangeView.as_view(), name="verify-old-password"), # обновление пароля через профиль – ввод старого пароля
    path(r'users/change_password/', SetNewPasswordAPIView.as_view(), name="password-change"),  #  обновление пароля (из лк)
    path('users/password_reset_complete/<str:email>/<code>/', SetNewPasswordAPIView.as_view(),  # восстановление пароля (при входе)
         name='password-reset-complete'),
    path("", index, name="index"),
]
