import boto3
import os
from botocore.exceptions import ClientError

SENDER = os.getenv('SENDER_NAME')

RECIPIENT = os.getenv('LOGIN_EMAIL')

AWS_REGION = "us-east-2"

SUBJECT = "Amazon SES Test (SDK for Python)"

BODY_HTML = """<html>
<head></head>
<body>
  <h1>Amazon SES Test (SDK for Python)</h1>
  <p>This email was sent with
    <a href='https://aws.amazon.com/ses/'>Amazon SES</a> using the
    <a href='https://aws.amazon.com/sdk-for-python/'>
      AWS SDK for Python (Boto)</a>.</p>
</body>
</html>
            """

CHARSET = "UTF-8"

client = boto3.client('ses',
                      region_name="us-east-2",
                      aws_access_key_id=os.getenv('AWS_ACCESS_KEY_ID'),
                      aws_secret_access_key=os.getenv('AWS_SECRET_ACCESS_KEY'))

def send_email(email=RECIPIENT, body=BODY_HTML, theme=SUBJECT, sender=None):
    if not email:
        email = RECIPIENT
    if not sender:
        sender = SENDER
    print(email, body, theme, sender)
    try:
        response = client.send_email(
            Destination={
                'ToAddresses': [
                    email,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': CHARSET,
                        'Data': body,
                    }
                },
                'Subject': {
                    'Charset': CHARSET,
                    'Data': theme,
                },
            },
            Source=sender,
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    except Exception as e:
        print(e)
    else:
        print("Email sent! Message ID:"),
        print(response)
