from django.shortcuts import HttpResponse
import telebot
from django.conf import settings
import os
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import get_user_model
import requests
from classes.models import GameRegister, get_closest_game, Game, GameTypes
import datetime

bot = telebot.TeleBot(settings.BOT_TOKEN)

@csrf_exempt
def index(request):
    if request.method == "POST":
        update = telebot.types.Update.de_json(request.body.decode('utf-8'))
        bot.process_new_updates([update])
    return HttpResponse('ok')

# удаление клавиатуры
def delete_markup(message):
    bot.edit_message_reply_markup(message.chat.id, message.message_id)

# обработка старта
@bot.message_handler(commands=['start'])
def start(message):
    User = get_user_model()
    if User.objects.filter(tg_bot_id=message.chat.id).exists():
        bot.send_message(message.chat.id, 'Вы уже авторизованы!')
        menu(message)
    else:
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Я участник клуба', callback_data=1))
        markup.add(telebot.types.InlineKeyboardButton(text='Как попасть в клуб?', callback_data=2))
        markup.add(telebot.types.InlineKeyboardButton(text='Что такое клуб переговоров?', callback_data=3))
        bot.send_message(message.chat.id, 'Привет! Я бот клуба переговоров. Чем могу помочь?', reply_markup=markup)

# проверка что человек зареган
def i_am_member(message):
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='Да', callback_data=4))
    markup.add(telebot.types.InlineKeyboardButton(text='Нет', callback_data=5))
    bot.send_message(message.chat.id, 'Отлично! Регистрировались ли вы ранее на нашем сайте?', reply_markup=markup)

# авторизация
def authorization(message, flag=False):
    if flag:
        bot.send_message(message.chat.id, 'Напишите почту, с которой ты регистрировался на сайте:')
    else:
        bot.send_message(message.chat.id, 'Здорово! Нужно только авторизоваться.\n\nНапишите почту, с которой ты регистрировался на сайте:')
    bot.register_next_step_handler(message, get_email)

# обработка почты
def get_email(message):
    email = message.text
    bot.send_message(message.chat.id, 'И свой пароль:')
    bot.register_next_step_handler(message, get_password, email)

# обработка пароля
def get_password(message, email):
    password = message.text
    response = requests.post(f'{settings.DOMAIN_NAME}/users/auth/bot_login/', data={'email': email, 'password': password})
    if response.status_code == 200:
        bot.send_message(message.chat.id, 'Авторизация прошла успешно!\n\nСчастливых вам игр, и пусть удача, всегда будет с вами!')
        User = get_user_model()
        user = User.objects.get(email=email)
        user.tg_bot_id = message.chat.id
        user.save()
        menu(message)
    else:
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Попробовать снова', callback_data=7))
        markup.add(telebot.types.InlineKeyboardButton(text='Перейти к регистрации на сайте', url='https://www.google.com'))
        markup.add(telebot.types.InlineKeyboardButton(text='Перейти в меню', callback_data=6))
        bot.send_message(message.chat.id, 'Участника с такими данными нет в зарегестрированных.', reply_markup=markup)

# основное меню
def menu(message):
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='Записаться на ближайшее занятие', callback_data=8))
    markup.add(telebot.types.InlineKeyboardButton(text='Моя запись', callback_data=9))
    markup.add(telebot.types.InlineKeyboardButton(text='Правила клуба', url='https://www.google.com'))
    markup.add(telebot.types.InlineKeyboardButton(text='Выйти из аккаунта', callback_data=11))
    bot.send_message(message.chat.id, 'Чем я могу помочь?', reply_markup=markup)

# запись на игру
def register(message, flag=True):
    User = get_user_model()
    game_register = None
    if flag:
        game_register = GameRegister.objects.create(player=User.objects.get(tg_bot_id=message.chat.id))
    else:
        game_register = GameRegister.objects.get(player=User.objects.get(tg_bot_id=message.chat.id), date=get_closest_game())
    date = game_register.date
    address = 'Address'
    time_start = game_register.begin_time
    time_finish = game_register.finish_time
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='Подтвердить запись', callback_data=12))
    markup.add(telebot.types.InlineKeyboardButton(text='Изменить время присутствия', callback_data=13))
    markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=14))
    bot.send_message(message.chat.id, f'Ближайшее занятие {date.strftime("%d.%m.%Y")}\nВремя: 16:30-21:00\nАдрес {address}\nВремя присутствия: 16:30-21:00', reply_markup=markup)

# изменение времени прихода
def change_reg_time(message, flag=True):
    markup = telebot.types.ReplyKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='Приду вовремя (16:30)'))
    markup.add(telebot.types.InlineKeyboardButton(text='17:00'))
    markup.add(telebot.types.InlineKeyboardButton(text='17:30'))
    markup.add(telebot.types.InlineKeyboardButton(text='18:00'))
    markup.add(telebot.types.InlineKeyboardButton(text='18:30'))
    markup.add(telebot.types.InlineKeyboardButton(text='19:00'))
    markup.add(telebot.types.InlineKeyboardButton(text='19:30'))
    markup.add(telebot.types.InlineKeyboardButton(text='20:00'))
    markup.add(telebot.types.InlineKeyboardButton(text='20:30'))
    markup.add(telebot.types.InlineKeyboardButton(text='Назад'))
    bot.send_message(message.chat.id, 'Выберите время прихода:', reply_markup=markup)
    #отслеживать следующее сообщение
    bot.register_next_step_handler(message, get_reg_time, flag)

# обработка времени прихода
def get_reg_time(message, flag=True):
    #проверить что сообщение это текст одной из кнопок
    if message.text == 'Назад':
        # отправить сообщение и удалить клавиатуру
        bot.send_message(message.chat.id, 'Назад', reply_markup=telebot.types.ReplyKeyboardRemove())
        # удалить сообщение
        bot.delete_message(message.chat.id, message.message_id+1)
        if flag:
            register(message, False)
        else:
            my_reg(message)
    elif message.text in ('Приду вовремя (16:30)', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30'):
        #перевести время в datetime
        if message.text == 'Приду вовремя (16:30)':
            start_time = datetime.datetime.strptime('16:30', '%H:%M').time()
        else:
            start_time = datetime.datetime.strptime(message.text, '%H:%M').time()
        change_reg_finish_time(message, start_time, flag)
    else:
        bot.send_message(message.chat.id, 'Некорректный ввод. Попробуйте еще раз.')
        change_reg_time(message, flag)

# изменение времени ухода
def change_reg_finish_time(message, start_time, flag=True):
    markup = telebot.types.ReplyKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='17:00'))
    markup.add(telebot.types.InlineKeyboardButton(text='17:30'))
    markup.add(telebot.types.InlineKeyboardButton(text='18:00'))
    markup.add(telebot.types.InlineKeyboardButton(text='18:30'))
    markup.add(telebot.types.InlineKeyboardButton(text='19:00'))
    markup.add(telebot.types.InlineKeyboardButton(text='19:30'))
    markup.add(telebot.types.InlineKeyboardButton(text='20:00'))
    markup.add(telebot.types.InlineKeyboardButton(text='20:30'))
    markup.add(telebot.types.InlineKeyboardButton(text='Остаюсь до конца (21:00)'))
    markup.add(telebot.types.InlineKeyboardButton(text='Назад'))
    bot.send_message(message.chat.id, 'Выберите время ухода:', reply_markup=markup)
    #отслеживать следующее сообщение
    bot.register_next_step_handler(message, get_reg_finish_time, start_time, flag)

# обработка времени ухода
def get_reg_finish_time(message, start_time, flag=True):
    #проверить что сообщение это текст одной из кнопок
    if message.text == 'Назад':
        change_reg_time(message, flag)
    elif message.text in ('17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30', 'Остаюсь до конца (21:00)'):
        if message.text == 'Остаюсь до конца (21:00)':
            finish_time = datetime.datetime.strptime('21:00', '%H:%M').time()
        else:
            finish_time = datetime.datetime.strptime(message.text, '%H:%M').time()
        accept(message, start_time, finish_time, flag)
    else:
        bot.send_message(message.chat.id, 'Некорректный ввод. Попробуйте еще раз.')
        change_reg_finish_time(message, start_time, flag)

# подтверждение изменения времени
def accept(message, start_time, finish_time, flag=True):
    User = get_user_model()
    GameRegister.objects.filter(player=User.objects.get(tg_bot_id=message.chat.id), date=get_closest_game()).update(begin_time=start_time, finish_time=finish_time)
    if flag:
        bot.send_message(message.chat.id, message.text, reply_markup=telebot.types.ReplyKeyboardRemove())
        bot.delete_message(message.chat.id, message.message_id+1)
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Подтвердить', callback_data=12))
        bot.send_message(message.chat.id, 'Время присутствия на занятии успешно изменено!\n\nЧтобы записаться на занятие подвердите запись.', reply_markup=markup)
    else:
        bot.send_message(message.chat.id, 'Время присутствия на занятии успешно изменено!', reply_markup=telebot.types.ReplyKeyboardRemove())
        my_reg(message)

# моя запись
def my_reg(message):
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='Изменить время присутствия', callback_data=15))
    markup.add(telebot.types.InlineKeyboardButton(text='Посмотреть данные по предстоящей игре', callback_data=16))
    markup.add(telebot.types.InlineKeyboardButton(text='Отменить запись', callback_data=17))
    markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=18))
    User = get_user_model()
    game_register = GameRegister.objects.get(player=User.objects.get(tg_bot_id=message.chat.id), date=get_closest_game())
    date = game_register.date
    address = 'Address'
    time_start = game_register.begin_time.strftime('%H:%M')
    time_finish = game_register.finish_time.strftime('%H:%M')
    bot.send_message(message.chat.id, f'Ближайшее занятие {date.strftime("%d.%m.%Y")}\nВремя: 16:30-21:00\nАдрес {address}\nВремя присутствия: {time_start}-{time_finish}', reply_markup=markup)

# отмена записи
def cancel_reg(message):
    # проверить день сейчас если это пятница или суббота
    User = get_user_model()
    player_from_reg = GameRegister.objects.get(player=User.objects.get(tg_bot_id=message.chat.id), date=get_closest_game())
    game = Game.objects.filter(date=get_closest_game()) & (Game.objects.filter(player1=player_from_reg) | Game.objects.filter(player2=player_from_reg))
    if datetime.datetime.now().weekday() in (4, 5) or game.filter(game_type=GameTypes.CONFLICT).exists() or game.filter(game_type=GameTypes.DISCUSSION).exists():
        # две кнопки
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Связаться с руководителем', url='https://t.me/soldatov_sem'))
        markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=20))
        bot.send_message(message.chat.id, 'К сожалению, я не могу отменить запись, так как сетка уже составлена. Для отмены свяжитесь с руководителем.', reply_markup=markup)
    else:
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Да', callback_data=19))
        markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=20))
        bot.send_message(message.chat.id, 'Уверены, что хотите отменить запись?', reply_markup=markup)

# посмотреть данные по игре
def show_game_data(message):
    # проверить существуют ли записи в бд Game для ближайшей даты и айди человека с типом конфликты
    User = get_user_model()
    player_from_reg = GameRegister.objects.get(player=User.objects.get(tg_bot_id=message.chat.id), date=get_closest_game())
    game = Game.objects.filter(date=get_closest_game()) & (Game.objects.filter(player1=player_from_reg) | Game.objects.filter(player2=player_from_reg))
    text = ""
    if game.exists():
        if game.filter(game_type=GameTypes.CONFLICT).exists():
            conflict = game.filter(game_type=GameTypes.CONFLICT)
            for instance in conflict:
                text += f"*{instance.game_type}*\nСтол: {instance.table.number}\nОппонент: {instance.player2.player.first_name if instance.player1 == player_from_reg else instance.player1.player.first_name} {instance.player2.player.last_name if instance.player1 == player_from_reg else instance.player1.player.last_name}\n"
                text += '\n'
        if game.filter(game_type=GameTypes.DISCUSSION).exists():
            discussion = game.filter(game_type=GameTypes.DISCUSSION)
            for instance in discussion:
                text += f"*{instance.game_type}*\nСтол: {instance.table.number}\nОппонент: {instance.player2.player.first_name if instance.player1 == player_from_reg else instance.player1.player.first_name} {instance.player2.player.last_name if instance.player1 == player_from_reg else instance.player1.player.last_name}\n"
                text += '\n'
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Посмотреть сетку', url='https://www.google.com'))
        markup.add(telebot.types.InlineKeyboardButton(text='Переговорный кейс', callback_data=21))
        markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=20))
        bot.send_message(message.chat.id, text, parse_mode='Markdown', reply_markup=markup)
    else:
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=20))
        bot.send_message(message.chat.id, 'Сетка еще не составлена. \nЯ обязательну сообщу, как только она будет готова!', reply_markup=markup)

# вывод кейса
def show_case(message):
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=16))
    User = get_user_model()
    player_from_reg = GameRegister.objects.get(player=User.objects.get(tg_bot_id=message.chat.id), date=get_closest_game())
    game = Game.objects.filter(date=get_closest_game()) & (Game.objects.filter(player1=player_from_reg) | Game.objects.filter(player2=player_from_reg))
    text = ""
    if game.filter(game_type=GameTypes.DISCUSSION).exists():
        discussion = game.filter(game_type=GameTypes.DISCUSSION)
        for instance in discussion:
            text += f"*{instance.case.number} {instance.case.name}*\n{instance.case.label.all()[0].type}\n\n{instance.case.text}\n"
            text += '\n'
    else:
        text = "Вам не назначен переговорный кейс"
    bot.send_message(message.chat.id, text, reply_markup=markup, parse_mode='Markdown')

# уведомление о записи
def notification_register():
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='Записаться на ближайшее занятие', callback_data=8))
    User = get_user_model()
    date = get_closest_game()
    # взять игроков у кого существует tg_bot_id
    players = User.objects.filter(tg_bot_id__isnull=False)
    for player in players:
        if not GameRegister.objects.filter(player=player, date=date).exists():
            bot.send_message(player.tg_bot_id, f'Следующее занятие {date.strftime("%d.%m.%Y")} \nСкорее записывайтесь!', reply_markup=markup)


@bot.message_handler(commands=['send_notification'])
def send_notification_register(message):
    if message.chat.id == 449567677:
        notification_register()
    else:
        bot.send_message(message.chat.id, 'У вас нет прав на выполнение данной команды')


def notification_ready_game():
    markup = telebot.types.InlineKeyboardMarkup()
    markup.add(telebot.types.InlineKeyboardButton(text='Посмотреть данные по предстоящей игре', callback_data=16))
    games = Game.objects.filter(date=get_closest_game())
    tg_id_list = []
    for game in games:
        if game.player1.player.tg_bot_id:
            if game.player1.player.tg_bot_id not in tg_id_list:
                tg_id_list.append(game.player1.player.tg_bot_id)
        if game.player2.player.tg_bot_id:
            if game.player2.player.tg_bot_id not in tg_id_list:
                tg_id_list.append(game.player2.player.tg_bot_id)
    for tg_id in tg_id_list:
        bot.send_message(tg_id, 'Сетка готова! Не забудь подготовиться к переговорной ситуации!', reply_markup=markup)


@bot.message_handler(commands=['send_notification_ready_game'])
def send_notification_ready_game(message):
    if message.chat.id == 449567677:
        notification_ready_game()
    else:
        bot.send_message(message.chat.id, 'У вас нет прав на выполнение данной команды')

# обработка всех инлайн кнопок
@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.data == '1':
        delete_markup(call.message)
        i_am_member(call.message)
    elif call.data == '2':
        delete_markup(call.message)
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Перейти к регистрации на сайте', url='https://www.google.com'))
        markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=6))
        bot.send_message(call.message.chat.id, 'Заполни заявку для вступления в клуб на нашем сайте!', reply_markup=markup)
    elif call.data == '3':
        bot.send_message(call.message.chat.id, 'Клуб переговоров - это ...')
    elif call.data == '4':
        delete_markup(call.message)
        authorization(call.message)
    elif call.data == '5':
        delete_markup(call.message)
        markup = telebot.types.InlineKeyboardMarkup()
        markup.add(telebot.types.InlineKeyboardButton(text='Перейти к регистрации на сайте', url='https://www.google.com'))
        markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=6))
        bot.send_message(call.message.chat.id, 'Бот предназначен для зарегестрированных участников.\n\nСкорее регистрируйтесь и возвращайтесь!', reply_markup=markup)
    elif call.data == '6':
        delete_markup(call.message)
        start(call.message)
    elif call.data == '7':
        delete_markup(call.message)
        authorization(call.message, True)
    elif call.data == '8':
        delete_markup(call.message)
        User = get_user_model()
        if GameRegister.objects.filter(player=User.objects.get(tg_bot_id=call.message.chat.id), date=get_closest_game()).exists():
            bot.send_message(call.message.chat.id, 'Вы уже записаны на занятие!')
            menu(call.message)
        elif datetime.datetime.now().weekday() in (4, 5):
            bot.send_message(call.message.chat.id, 'К сожалению, запись на занятие уже прошла. Скоро откроется новая, я обязательно сообщу как только она появится!')
            menu(call.message)
        else:
            register(call.message)
    elif call.data == '9':
        delete_markup(call.message)
        # проверить существование записи
        User = get_user_model()
        if GameRegister.objects.filter(player=User.objects.get(tg_bot_id=call.message.chat.id), date=get_closest_game()).exists():
            my_reg(call.message)
        else:
            bot.send_message(call.message.chat.id, 'Вы ещё не записаны на занятие!')
            menu(call.message)
    elif call.data == '11':
        delete_markup(call.message)
        User = get_user_model()
        user = User.objects.get(tg_bot_id=call.message.chat.id)
        user.tg_bot_id = None
        user.save()
        start(call.message)
    elif call.data == '12':
        delete_markup(call.message)
        bot.send_message(call.message.chat.id, 'Запись подтверждена! Ждем вас на занятии!', reply_markup=telebot.types.ReplyKeyboardRemove())
        menu(call.message)
    elif call.data == '13':
        delete_markup(call.message)
        change_reg_time(call.message)
    elif call.data == '14':
        delete_markup(call.message)
        User = get_user_model()
        #удаление только что созданной записи по id и ближайшей игре
        GameRegister.objects.filter(player=User.objects.get(tg_bot_id=call.message.chat.id), date=get_closest_game()).delete()
        menu(call.message)
    elif call.data == '15':
        # проверка даты
        delete_markup(call.message)
        if datetime.datetime.now().weekday() in (4, 5):
            markup = telebot.types.InlineKeyboardMarkup()
            markup.add(telebot.types.InlineKeyboardButton(text='Связаться с руководителем', url='https://t.me/soldatov_sem'))
            markup.add(telebot.types.InlineKeyboardButton(text='Назад', callback_data=20))
            bot.send_message(call.message.chat.id, 'К сожалению, я не могу изменить время записи, так как сетка уже составлена. Для изменения времени свяжитесь с руководителем.', reply_markup=markup)
        else:
            change_reg_time(call.message, False)
    elif call.data == '16':
        delete_markup(call.message)
        show_game_data(call.message)
    elif call.data == '17':
        delete_markup(call.message)
        cancel_reg(call.message)
    elif call.data == '18':
        delete_markup(call.message)
        menu(call.message)
    elif call.data == '19':
        delete_markup(call.message)
        User = get_user_model()
        GameRegister.objects.filter(player=User.objects.get(tg_bot_id=call.message.chat.id), date=get_closest_game()).delete()
        bot.send_message(call.message.chat.id, 'Запись на занятие отменена!', reply_markup=telebot.types.ReplyKeyboardRemove())
        menu(call.message)
    elif call.data == '20':
        delete_markup(call.message)
        my_reg(call.message)
    elif call.data == '21':
        delete_markup(call.message)
        show_case(call.message)