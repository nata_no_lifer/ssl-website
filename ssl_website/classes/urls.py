from django.urls import path, include

# from users.views import login, registration, profile, logout
# from .views import SignUp, profile_settings, logout
from .views import GameRegistration, TableView, GameProcess, GameLabelView, CasesView, RefereeView, \
    send_games, check_players_table, get_info_table
from rest_framework.routers import DefaultRouter

app_name = 'classes'
router = DefaultRouter()
router.register('game_register', GameRegistration, basename="game-registration") # регистрация пользователя на игру / изменение регистрации
router.register('table', TableView, basename='table') # все что связано с работой со столом (где стол это id, аудитория и дата)
router.register('game', GameProcess, basename='game-process') # составление пар игроков, процесс игры
router.register('referee', RefereeView, basename='referee') # назначение арбитров
router.register('cases', CasesView, basename='cases') # работа с кейсами
router.register('game_labels', GameLabelView, basename='game-labels') # работа с типами кейсов
# print(router.urls)
# get_games_table + функция возвращяющая арбитров от стола
urlpatterns = [
    # path(r'classes/edit_games/<int:number>', GameEditView.as_view(), name='games-edit') – future url for rendering
    # page with game
    # path(r'classes/edit_games/', GameEditView.as_view(), name='games-edit') – future url for
    # rendering page with saturday's games
    path(r'classes/send_games/', send_games, name='send-games'), # отправка сетки всем (с уведомлением участников)
    path('classes/get_info_table/', get_info_table, name='get-info-table-no-id'),
    path(r'classes/get_info_table/<int:table_id>/', get_info_table, name='get-info-table'),
    path(r'classes/table/get_players/<int:id>/', check_players_table, name='check-players-on-table'), # отправка сетки всем (с уведомлением участников)
    path(r'classes/', include(router.urls)), # подключение роутеров
]

# print(router.urls)
