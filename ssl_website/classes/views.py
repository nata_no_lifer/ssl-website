import collections
import json
import uuid
from django.contrib import auth
from django.contrib.auth import get_user_model, password_validation, authenticate, login
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import get_object_or_404
from users.permissions import ReadOnlyPermission, ArbitratorPermission, AdminPermission, StudentPermission
from users.models import UserRole
from users.serializers import UserAllSerializer
from users.views import UserEncoder
from django.db.models import Q
from django.forms.models import model_to_dict
from rest_framework import viewsets, status, generics, views
from rest_framework.views import APIView
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from json import JSONEncoder
from .models import Cases, GameRegister, Game, Referee, get_closest_game, GameTypes
from .serializers import *
from ssl_website.emails import send_email
from drf_yasg.utils import swagger_auto_schema
User = get_user_model()


def return_fields(field, object):
    if object:

        game_registration = GameRegister.objects.get(id=object[field])
        user = User.objects.filter(id=game_registration.player.id).values()[0]
        user_serializer = UserAllSerializer(user)
        game_reg_serialiser = GameRegistrationSerializer(game_registration)
        return {'user':user_serializer.data, 'game_registration': game_reg_serialiser.data}
    return {f'{field}': None}

# Create your views here.
class GameRegistration(viewsets.ModelViewSet):
    queryset = GameRegister.objects.all()
    serializer_class = GameRegistrationSerializer
    permission_classes_by_action = {
        'list': [AdminPermission | ArbitratorPermission | StudentPermission],
        'create': [StudentPermission | ArbitratorPermission | AdminPermission],
        'retrieve': [AdminPermission | ArbitratorPermission | StudentPermission],
        'partial_update': [AdminPermission | ArbitratorPermission | StudentPermission],
        'destroy': [AdminPermission | ArbitratorPermission | StudentPermission],
    }
    # `create()`, `retrieve()`, `update()`,
    #     `partial_update()`, `destroy()` and `list()` actions.
    def list(self, request):
        # add redirect if userrole == student
        if request.user.role != UserRole.ADMIN and request.user.role != UserRole.ARBITRATOR:
            url = f"{settings.DOMAIN_NAME}{reverse('classes:game-registration-detail', kwargs={'pk': request.user.pk})}"
            return HttpResponseRedirect(redirect_to=url)
        game_day = get_closest_game()
        queryset = GameRegister.objects.filter(date=game_day)
        # print(queryset)
        serializer = self.serializer_class(queryset, many=True)
        for obj in serializer.data:
            user = User.objects.filter(id=obj['player']).values()[0]
            obj['player'] = UserAllSerializer(user).data
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        data = json.dumps(request.data)
        data = json.loads(data)
        if 'player' not in data:
            data.update({'player': request.user.id})
        if request.user.role == UserRole.STUDENT and \
                datetime.datetime.now().weekday() in (4, 5):
                return Response({"Error": "Вы не можете зарегистрироваться на игру, так как дедлайн прошел. "
                                          "Регистрация на следующую игру откроется в совкресение."},
                                status=status.HTTP_405_METHOD_NOT_ALLOWED)
        serializer = self.serializer_class(data=data)
        # print(serializer)
        if not serializer.is_valid():
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def partial_update(self, request, pk=None, *args, **kwargs):
        if pk: pk = int(pk)
        print(request)
        entry = get_object_or_404(GameRegister, pk=pk)
        user = entry.player
        print(user.pk, request.user.pk)
        if request.user.role not in [UserRole.ADMIN, UserRole.ARBITRATOR] and request.user.pk != user.pk:
            return Response({"Error": "У вас нет доступа для просмотра данной страницы"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)
        if request.user.role == UserRole.STUDENT and \
                datetime.datetime.now().weekday() in (4, 5):
                return Response({"Error": "Вы не можете редактировать данные о регистрации на игру, так как дедлайн прошел. "
                                          "Регистрация на следующую игру откроется в совкресение."},
                                status=status.HTTP_405_METHOD_NOT_ALLOWED)
        serializer = self.serializer_class(entry, data=request.data, partial=True, context={'request': request})
        if not serializer.is_valid():
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
        # print(serializer)
        self.perform_update(serializer)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None, *args, **kwargs):
        if pk:
            pk = int(pk)
        user = request.user
        if user.role == UserRole.STUDENT:
            try:
                entry = GameRegister.objects.get(Q(player=user.id) & Q(date=get_closest_game()))
            except:
                return Response({'Error': "Вы еще не регистрировались на ближайшую игру"}, status=status.HTTP_404_NOT_FOUND)
        else:
            entry = get_object_or_404(GameRegister, pk=pk)
        if request.user.role not in [UserRole.ADMIN, UserRole.ARBITRATOR] and request.user.pk != entry.player.pk:
            return Response({"Error": "У вас нет доступа для просмотра данной страницы"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)
        serializer = self.serializer_class(entry)
        data = serializer.data
        user = User.objects.filter(id=data['player']).values()[0]
        data['player'] = UserAllSerializer(user).data
        return Response(data, status=status.HTTP_200_OK)

    def destroy(self, request, pk=None, *args, **kwargs):
        if pk: pk = int(pk)
        entry = get_object_or_404(GameRegister, pk=pk)
        user = entry.player
        print(user.pk, request.user.pk)
        if request.user.role not in [UserRole.ADMIN, UserRole.ARBITRATOR] and request.user.pk != user.pk:
            return Response({"Error": "У вас нет прав для совершения данного действия"},
                            status=status.HTTP_405_METHOD_NOT_ALLOWED)
        if datetime.datetime.now().weekday() in (4, 5) and request.user.role == UserRole.STUDENT:
            return Response({'Error': "Дедлайн удаления записи прошел."})
        self.perform_destroy(entry)
        return Response(status=status.HTTP_204_NO_CONTENT)


    def get_permissions(self):
        try:
            # return permission_classes depending on `action`
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            # action is not set return default permission_classes
            return [permission() for permission in self.permission_classes]

class GameProcess(viewsets.ModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    permission_classes_by_action = {
        'list': [AdminPermission | ArbitratorPermission],
        'create': [AdminPermission | ArbitratorPermission],
        'retrieve': [AdminPermission | ArbitratorPermission],
        'partial_update': [AdminPermission | ArbitratorPermission],
        'destroy': [AdminPermission | ArbitratorPermission],
        'get_games_table': [AdminPermission | ArbitratorPermission],
        'last_games': [ArbitratorPermission | StudentPermission]
    }

    def list(self, request):
        queryset = Game.objects.filter(date=get_closest_game())
        serializer = self.serializer_class(queryset, many=True)
        for obj in serializer.data:
            # print(obj)

            if obj['player1']:
                obj['player1'] = return_fields('player1', obj)
            if obj['player2']:
                obj['player2'] = return_fields('player2', obj)
            if obj['case']:
                obj['case'] = CasesSerializer(Cases.objects.get(id=obj['case'])).data

            # print(obj['table'])
            table = Table.objects.get(id=obj['table'])
            obj['table'] = table.number
        return Response(serializer.data, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        data = json.dumps(request.data)
        data = json.loads(data)
        if 'table' not in data:
            table = Table.objects.filter(date=data.get('date', get_closest_game())).last()
        else:
            table = Table.objects.get(id=data['table'])
        data['table'] = table.id
        print(data)
        serializer = self.serializer_class(data=data)
        if not serializer.is_valid():
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        data = serializer.data
        if data['player1']:
            data['player1'] = return_fields('player1', data)
        if data['player1']:
            data['player2'] = return_fields('player2', data)
        if data['case']:
            data['case'] = CasesSerializer(Cases.objects.get(id=data['case'])).data
        return Response(data, status=status.HTTP_201_CREATED)

    def retrieve(self, request, pk=None, *args, **kwargs):
        if pk: pk = int(pk)
        entry = get_object_or_404(Game, pk=pk)
        serializer = self.serializer_class(entry)
        data = serializer.data
        if data['player1']:
            data['player1'] = return_fields('player1', data)
        if data['player2']:
            data['player2'] = return_fields('player2', data)
        if data['case']:
            data['case'] = CasesSerializer(Cases.objects.get(id=data['case'])).data

        return Response(data, status=status.HTTP_200_OK)

    def partial_update(self, request, pk=None, *args, **kwargs):
        if pk: pk = int(pk)
        entry = get_object_or_404(Game, pk=pk)
        serializer = self.serializer_class(entry, data=request.data, partial=True, context={'request': request})
        if not serializer.is_valid():
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
        # print(serializer)
        self.perform_update(serializer)
        data = serializer.data
        # print(data, 'kkk')
        if data['player1']:
            data['player1'] = return_fields('player1', data)
        if data['player2']:
            data['player2'] = return_fields('player2', data)
        return Response(data, status=status.HTTP_200_OK)

    @action(methods=['GET',], detail=False)
    def last_games(self, request):  # функция для просмотра прошедших игр (студент и арбитр)
        user = request.user
        print(user)
        data = []
        try:
            queryset = Game.objects.filter(Q(player1__player=user.id) | Q(player2__player=user.id))
            serializer = self.serializer_class(queryset, many=True)
            for obj in serializer.data:
                # print(obj)
                if obj['player1']:
                    obj['player1'] = return_fields('player1', obj)
                if obj['player2']:
                    obj['player2'] = return_fields('player2', obj)
                # print(obj['table'])
                table = Table.objects.get(id=obj['table'])
                obj['table'] = table.number
            data = serializer.data
        except:  # вопрос – надо ли добавлять арбитру столы на которых он судил?
            pass
        finally:
            if user.role == UserRole.ARBITRATOR:
                queryset = Referee.objects.filter(Q(referee__player=user.id))
                data.extend(RefereeSerializer(queryset, many=True).data)
        return Response(data, status=status.HTTP_200_OK)

    def get_permissions(self):
        try:
            return [permission() for permission in self.permission_classes_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]

class TableView(viewsets.ModelViewSet):
    serializer_class = TableSerializer
    permission_classes = [AdminPermission | ArbitratorPermission]
    queryset = Table.objects.filter(date=get_closest_game())

class GameLabelView(viewsets.ModelViewSet):
    serializer_class = GameLabelSerializer
    queryset = GameLabel.objects.all()
    permission_classes = [AdminPermission | ArbitratorPermission]

class CasesView(viewsets.ModelViewSet):
    serializer_class = CasesSerializer
    queryset = Cases.objects.all()
    permission_classes = [AdminPermission | ArbitratorPermission]

    @action(methods=['GET', ], detail=False)
    def cases_conflicts(self, request, *args, **kwargs):
        queryset = Cases.objects.filter(case_type=GameTypes.CONFLICT)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(methods=['GET', ], detail=False)
    def cases_discussion(self, request, *args, **kwargs):
        queryset = Cases.objects.filter(case_type=GameTypes.DISCUSSION)
        serializer = self.serializer_class(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=True)
        # print(serializer, request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

class RefereeView(viewsets.ModelViewSet):
    serializer_class = RefereeSerializer
    permission_classes = [AdminPermission | ArbitratorPermission]
    queryset = Referee.objects.filter(date=get_closest_game())

    def list(self, request):
        serializer = self.serializer_class(self.queryset, many=True)
        for obj in serializer.data:
            obj['referee'] = return_fields('referee', obj)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def retrieve(self, request, pk=None, *args, **kwargs):
        if pk: pk = int(pk)
        entry = get_object_or_404(Referee, pk=pk)
        serializer = self.serializer_class(entry)
        data = serializer.data
        data['referee'] = return_fields('referee', data)
        return Response(data, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        # request.query_params
        # тут предполагается, что вместо table подается айди текущего стола,
        # но если это будет не так, надо переделать!
        print(request.data)
        data = json.dumps(request.data)
        data = json.loads(data)
        if 'table' not in data:
            table = Table.objects.filter(date=data.get('date', get_closest_game())).last()
        else:
            table = get_object_or_404(Table, id=data['table'])
        print(table)
        data['table'] = [table.id]
        serializer = self.serializer_class(data=data)
        if not serializer.is_valid():
            return Response({'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        data = serializer.data
        data['referee'] = return_fields('referee', data)
        return Response(data, status=status.HTTP_201_CREATED)

@api_view(['POST', 'GET', ])
def send_games(request):
    games = Game.objects.filter(date=request.data.get('date', get_closest_game()))
    print(games)
    players = []
    for game in games:
        for user in [game.player1.player, game.player2.player]:
            if user in players:
                continue
            link = reverse('classes:game-process-last-games')
            verification_link = f'{settings.DOMAIN_NAME}{link}'
            subject = f'Сетка заполнена'
            message = f'Сетка на ближайшую игру заполнена. \n' \
                      f'Для детального просмотра перейдите по ссылке: {verification_link}'
            send_email(
                theme=subject,
                body=message,
                email=user.email
            )
            players.append(user)

    return Response({'Message': 'Сетка отправлена участникам'},status=status.HTTP_200_OK)

@api_view(['GET', ])
@permission_classes([AdminPermission | ArbitratorPermission])
def check_players_table(request, id=None, **kwargs): # проверка находятся ли люди на столе (для корректного удаления стола)
    print(id)
    players = Game.objects.filter(table=id)
    referees = Referee.objects.filter(table=id)
    if len(players) > 0 or len(referees) > 0:
        return Response({'Alert': "За столом есть участники"}, status=status.HTTP_302_FOUND)
    return Response({'Message': "Стол пуст"}, status=status.HTTP_200_OK)

def show_table_info(table_id):
    table = get_object_or_404(Table, pk=table_id)
    # closest_game = get_closest_game()
    referees = Referee.objects.filter(table=table_id)
    referee_serializer = RefereeSerializer(referees, many=True)
    conflicts = Game.objects.filter(table=table_id, game_type=GameTypes.CONFLICT)
    conflicts_serializer = GameSerializer(conflicts, many=True)
    discussions = Game.objects.filter(table=table_id, game_type=GameTypes.DISCUSSION)
    discussions_serializer = GameSerializer(discussions, many=True)
    table_serializer = TableSerializer(table)
    for obj in conflicts_serializer.data:
        if obj['player1']:
            obj['player1'] = return_fields('player1', obj)
        if obj['player2']:
            obj['player2'] = return_fields('player2', obj)
        if obj['case']:
            obj['case'] = CasesSerializer(Cases.objects.get(id=obj['case'])).data
    for obj in discussions_serializer.data:
        if obj['player1']:
            obj['player1'] = return_fields('player1', obj)
        if obj['player2']:
            obj['player2'] = return_fields('player2', obj)
        if obj['case']:
            obj['case'] = CasesSerializer(Cases.objects.get(id=obj['case'])).data
    for obj in referee_serializer.data:
        if obj['referee']:
            obj['referee'] = return_fields('referee', obj)
    response_data = {
        "referees": referee_serializer.data,
        "conflicts": conflicts_serializer.data,
        "negotiations": discussions_serializer.data,
        'table': table_serializer.data
    }
    return response_data

@swagger_auto_schema(methods=['get'], operation_description="Method for returning information about all games on all tables (or 1 of them if identifying id) on the nearest saturday")
@api_view(['GET', ])
@permission_classes([AdminPermission | ArbitratorPermission])
def get_info_table(request, table_id=None, *args, **kwargs):
    print(request, table_id)
    if table_id:
        response_data = show_table_info(table_id)
    else:
        closest_game = get_closest_game()
        tables = Table.objects.filter(date=closest_game)
        response_data = []
        for table in tables:
            response_data.append(show_table_info(table.id))
    return Response(response_data, status=status.HTTP_200_OK)

