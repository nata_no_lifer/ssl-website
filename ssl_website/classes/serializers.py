from django.core.validators import RegexValidator
from django.shortcuts import get_object_or_404
from django.urls import reverse
from rest_framework import serializers
import datetime
from django.db.models import Q
from users.models import UserRole
from users.serializers import UserSerializer
from users.serializers import UserAllSerializer
from django.contrib.auth.password_validation import validate_password
import uuid
from datetime import timedelta
from django.utils.timezone import now
from rest_framework.reverse import reverse_lazy
from .models import Cases, GameRegister, Game, Referee, Table, get_closest_game, GameLabel, GameTypes

from django.conf import settings
from rest_framework.exceptions import AuthenticationFailed
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model, password_validation, authenticate, login
from django.core.mail import send_mail

User = get_user_model()


class GameRegistrationSerializer(serializers.ModelSerializer):
    all_time = serializers.BooleanField(required=False, label="Буду с 16:30 до 21:00")

    class Meta:
        model = GameRegister
        fields = ('id', 'date', 'all_time', 'begin_time', 'finish_time', 'player', 'attendance', 'count_games')

    def validate(self, attrs):
        if bool(attrs.get('all_time', 0)):
            return attrs
        if attrs.get('begin_time', datetime.time(16, 30)) > attrs.get('finish_time', datetime.time(21)) or \
                not datetime.time(16, 30) <= attrs.get('begin_time', datetime.time(16, 30)) <= datetime.time(21) or \
                not datetime.time(16, 30) <= attrs.get('finish_time', datetime.time(16, 30)) <= datetime.time(21):
            raise serializers.ValidationError({'error': "Диапазон допустимого времени с 16:30 до 21:00"})
        if 'player' in attrs and attrs.get('player').role == UserRole.USER:
            raise serializers.ValidationError({'error': "Данный студент не может участвовать в игре"})
        if len(GameRegister.objects.filter(Q(date=attrs.get('date', get_closest_game())) & \
                                           Q(player=attrs.get('player')))) > 0:
            raise serializers.ValidationError({'error': "Игрок уже зарегестрирован"})
        return attrs

    def create(self, validated_data):
        if 'all_time' in validated_data:
            del validated_data['all_time']
        return GameRegister.objects.create(**validated_data)


class GameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Game
        fields = ['id', 'date', 'case', 'player1', 'player1_score', 'player2', 'player2_score', 'table', 'game_type']


    def validate(self, attrs):
        if 'case' in attrs:
            case = attrs['case']
            # case = Cases.objects.get(id=attrs['case'])
            if 'game_type' in attrs:
                # print(attrs['game_type'])
                if case.case_type != attrs['game_type']:
                    raise serializers.ValidationError({'error': 'Тип кейса и игры не совпадают'})
            else:
                attrs['game_type'] = case.case_type
        else:
            attrs['case'] = None
        date = attrs.get('date', get_closest_game())
        if date < get_closest_game():
            raise serializers.ValidationError({'error': 'Дата указана не верно'})

        player_1 = attrs.get('player1', None)
        player_2 = attrs.get('player2', None)
        if player_1 and player_2 and player_1.player == player_2.player:
            raise serializers.ValidationError({'error': 'Выберите разных игроков'})
        if (player_1 and player_1.date != get_closest_game()) or (player_2 and player_2.date != get_closest_game()):
            raise serializers.ValidationError({'error': 'Выбрана регистрация на игру с неправильной датой'})

        return attrs

    def update(self, instance, validated_data):
        print('hahah', validated_data.get('case', instance.case))
        instance.case = validated_data.get('case', instance.case)
        print(instance.case)
        if 'game_type' in validated_data:
            if instance.case and instance.case.case_type != validated_data.get('game_type'):
                raise serializers.ValidationError({
                    'error': f'Тип кейса ({instance.case.case_type}) и игры ({validated_data.get("game_type")}) не совпадают'})
            else:
                if validated_data.get('game_type', instance.game_type) == 'Переговоры':
                    instance.game_type = GameTypes.DISCUSSION
                else:
                    instance.game_type = GameTypes.CONFLICT
        else:
            instance.game_type = GameTypes.CONFLICT
        if validated_data.get('player1', instance.player1) and validated_data.get('player2', instance.player2) and validated_data.get('player1', instance.player1) == validated_data.get('player2', instance.player2):
            raise serializers.ValidationError({'error': 'Выберите разных игроков'})
        # 'date','player1', 'player1_score', 'player2', 'player2_score', 'table']:
        instance.date = validated_data.get('date', instance.date)
        instance.player1 = validated_data.get('player1', instance.player1)
        instance.player1_score = validated_data.get('player1_score', instance.player1_score)
        instance.player2 = validated_data.get('player2', instance.player2)
        instance.player2_score = validated_data.get('player2_score', instance.player2_score)
        instance.table = validated_data.get('table', instance.table)
        instance.save()
        return instance


class RefereeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Referee
        fields = '__all__'

    def validate(self, attrs):
        if 'referee' in attrs:
            registrarion = attrs['referee']
            if registrarion.player.role != UserRole.ARBITRATOR:
                print(registrarion)
                raise serializers.ValidationError({'error': 'Выбранный игрок не является арбитром'})
        date = attrs.get('date', get_closest_game())
        if date < get_closest_game():
            raise serializers.ValidationError({'error': 'Дата указана не верно'})
        return attrs


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = '__all__'

    def validate(self, attrs):
        date = attrs.get('date', get_closest_game())
        if date < get_closest_game():
            raise serializers.ValidationError({'error': 'Дата указана не верно'})
        if 'number' in attrs:
            number = attrs['number']
            table = Table.objects.filter(Q(date=date) & Q(number=number)).last()
            if table:
                raise serializers.ValidationError({'error': 'Стол с таким номером уже занят'})
        return attrs

class GameLabelSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameLabel
        fields = '__all__'

class CasesSerializer(serializers.ModelSerializer):
    # label = GameLabelSerializer(many=True)

    class Meta:
        model = Cases
        fields = ('case_type', 'name', 'text', 'number', 'label')

    def to_representation(self, instance):
        representation = super().to_representation(instance)

        # Check the request method to decide how to represent the 'label' field
        # request = self.context.get('request')
        representation['label'] = self.get_label_as_strings(instance)

        return representation
    #
    def get_label_as_strings(self, instance):
        print(instance.label.all())
        return [label.type for label in instance.label.all()]
