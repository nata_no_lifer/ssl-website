import datetime

from django.contrib.auth import get_user_model
from django.core.validators import MinLengthValidator
from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
User = get_user_model()

def get_closest_game():
    """
    Функция для поиска даты ближайшей субботы
    """
    today = datetime.date.today()
    closest_sat = today + datetime.timedelta(6 - (today.weekday() + 1) % 7)
    return closest_sat

# Тип игр
class GameTypes(models.TextChoices):
    CONFLICT = "Конфликт"
    DISCUSSION = "Переговоры"


# Метки для игры, описывающие тип ситуации
class GameLabel(models.Model):
    type = models.CharField(max_length=100)


# посещаемость игрока – отметка пришел/опоздал/пропустил игру
class Attendance(models.TextChoices):
    PLAYED = "Сыграл"
    SKIP = "Не пришел"
    LATE = "Опоздал"

class Table(models.Model):
    number = models.CharField(default='1', null=False, max_length=100)
    date = models.DateField(null=False, default=get_closest_game())

    def __str__(self):
        return f'№{self.number} on {self.date}'

class GameRegister(models.Model):
    """
    Модель для регистрации игрока на игру
    Хранит в себе данные об игре
    Отображается в лк пользователя
    """
    date = models.DateField(null=False, default=get_closest_game())
    player = models.ForeignKey(to=User, on_delete=models.CASCADE, to_field="id")
    begin_time = models.TimeField(default=datetime.time(16, 30))
    finish_time = models.TimeField(default=datetime.time(21))
    attendance = models.CharField(
        max_length=30,
        choices=Attendance.choices,
        default=Attendance.PLAYED,
        verbose_name="Посещаемость",
    )
    count_games = models.IntegerField("Число игр", blank=False, default=0, validators=[MinValueValidator(0)])

    def __str__(self):
        return f'{self.player} | {self.date}'

class Cases(models.Model):
    """
    Модель для хранения кейсов
    (тип, название и текст кейса)
    """
    case_type = models.CharField(
        max_length=15,
        choices=GameTypes.choices,
        default=GameTypes.CONFLICT,
        verbose_name="Тип игры",
    )
    name = models.CharField("Название кейса", max_length=250, blank=True, default='')
    text = models.TextField("Текст кейса", null=False, validators=[MinLengthValidator(10)])
    number = models.IntegerField("Номер кейса", null=False, default=0)
    label = models.ManyToManyField(GameLabel, blank=True,verbose_name="Категория")

    def __str__(self):
        return f'{self.number} | {self.label.all()[0].type} | {self.case_type}'

class Game(models.Model):
    """
    Модель для создании игры
    Хранит в данные об игроках (связь с таблицей case) и конкретном кейсе (связь с таблицей Cases)
    Отображается в сетке
    (В дальнейшем будет использована для просмотра во вкладке "прошедшие игры")
    """
    date = models.DateField(null=False, default=get_closest_game())
    case = models.ForeignKey(Cases, related_name="case", on_delete=models.SET_NULL, null=True, to_field='id') # сделать связь с моделью Cases – ?
    player1 = models.ForeignKey(GameRegister, to_field="id", related_name="player_1", on_delete=models.SET_NULL, null=True)
    player1_score = models.IntegerField("Счет 1 игрока", default=0, null=False)
    player2 = models.ForeignKey(GameRegister,to_field="id", related_name="player_2", on_delete=models.SET_NULL, null=True)
    player2_score = models.IntegerField("Счет 2 игрока", default=0, null=False)
    table = models.ForeignKey(Table, related_name='Стол', to_field='id', on_delete=models.SET_NULL, null=True)
    game_type = models.CharField(
        max_length=15,
        choices=GameTypes.choices,
        default=GameTypes.CONFLICT,
        verbose_name="Тип игры",
    )


class Referee(models.Model):
    """
    Модель для хранении информации о судье
    Судья привязан к столу, дате и типу игры
    Связан с пользователями через поле referee
    """
    referee = models.ForeignKey(GameRegister, to_field="id", related_name="referee", on_delete=models.CASCADE)
    # game_info = models.ManyToManyField(Game)
    date = models.DateField(null=False, default=get_closest_game())
    table = models.ManyToManyField(Table, verbose_name='Стол')
