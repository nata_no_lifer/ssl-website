import { createSlice } from "@reduxjs/toolkit";
import { ParticipantProps } from "shared/types/participant";
import {TStatusType} from 'shared'
import { CaseProps } from "shared/types";
import CaseRequest from "../types/Case";
import Ru2EnType from "../lib/Ru2EnType";

const initialState: State = {
    casesMap: {},
    casesLoadingStatus: 'idle'
}

interface State {
    casesMap: { [id: number]: CaseProps}
    casesLoadingStatus: TStatusType
}


const casesSlice = createSlice({
    name: 'cases',
    initialState,
    reducers: {
        casesFetching: state => {state.casesLoadingStatus = 'loading'},

        casesFetched: (state, action: {payload: CaseRequest[]}) => {
            state.casesLoadingStatus = 'idle';
            state.casesMap = Object.fromEntries(action.payload.map((caseData: CaseRequest): [number, CaseProps] => ([caseData.id, {
                id: caseData.id,
                number: caseData.number,
                title: caseData.name,
                text: caseData.text,
                type: Ru2EnType(caseData.case_type),
                tags: caseData.label
            }])))
        },

        casesFetchingError: state => {
            state.casesLoadingStatus = 'error';
        },

        casesCreated: (state, action: {payload: CaseRequest}) => {
            let caseData: CaseRequest = action.payload;
            state.casesMap = {
                ...state.casesMap, 
                [caseData.id]: {
                    id: caseData.id,
                    number: caseData.number,
                    title: caseData.name,
                    text: caseData.text,
                    type: Ru2EnType(caseData.case_type),
                    tags: caseData.label
                }
            };
        }
    }
});

const {actions, reducer} = casesSlice;

export default reducer;
export const {
    casesFetching,
    casesFetched,
    casesFetchingError,
    casesCreated,
} = actions;