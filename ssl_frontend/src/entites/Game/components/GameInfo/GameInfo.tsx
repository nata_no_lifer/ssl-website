import { Participant,  Case, NewElementBtn } from 'shared';
import styles from './GameInfo.module.scss';
import { IGameInfo } from 'shared/types';
import classNames from 'classnames';
import Error from 'shared/ui/Error/Error';
import { useEffect, useState } from 'react';

interface GameInfoProps {
  pageType: "referee" | "admin"
  gameInfo: IGameInfo
  openModal: (participant: 'participant1' | 'participant2') => void
  openCaseModal: () => void
  handleDelete: (element: 'case' | 'participant1' | 'participant2') => void
  gameType: 'conflicts' | 'negotiations'
  handleSetScore: (element: 'player1Score' | 'player2Score', value: number) => void
}


function GameInfo(props: GameInfoProps) {
    const { gameInfo, openModal, openCaseModal, handleDelete, gameType, pageType, handleSetScore } = props;
    const participant1 = <Participant pageType={pageType} handleDelete={() => handleDelete('participant1')} participantData={gameInfo.participant1!} gameCounter = {gameType}/>
    const participant2 = <Participant pageType={pageType} handleDelete={() => handleDelete('participant2')} participantData={gameInfo.participant2!} gameCounter = {gameType}/>
    const error: boolean = Boolean(gameInfo.participant1 && gameInfo.participant2 && gameInfo.participant1.id === gameInfo.participant2.id)
    const errorMessage = 'Ошибка: игрок не может играть против самого себя'

    const [playersScore, setPlayersScore] = useState<[string, string]>(["", ""]);
    useEffect(() => {
        setPlayersScore([gameInfo.player1Score ? String(gameInfo.player1Score) : "",
                         gameInfo.player2Score ? String(gameInfo.player2Score) : ""])
    }, [gameInfo])
    return (
        <div className={styles.game__wrapper}>
            {gameInfo.participant1 === null 
            ? <NewElementBtn classNameProps={classNames({[styles.add_empty_participant_admin]: pageType === 'admin'},
            {[styles.add_empty_participant_referee]: pageType === 'referee'})}
            onClick={() => openModal('participant1')}/> 
            : <Error textChildren={errorMessage} isError={error} borderRadius = {19} left1 = {85}>{participant1}</Error> }

            {pageType === "referee" 
            ?   <div>
                    <input
                        value={playersScore[0]}
                        className={styles.score}
                        type="text"
                        maxLength={1}
                        pattern = "^0*[0-3]\d*$"
                        autoComplete="off"
                        onBlur={(e) => (e.target.value === "" || Number(e.target.value) > 0) && handleSetScore('player1Score', Number(e.target.value))}
                        onChange={(e) => setPlayersScore(prev => [e.target.value, prev[1]])}
                    />
                </div>
            : null}
            
            {gameInfo.case === null 
            ? <NewElementBtn classNameProps = {styles.add_empty_case} onClick={() => openCaseModal()}/> 
            : <Case btn_close='show' handleDelete={() => handleDelete('case')}>{gameInfo.case.number.toString()}</Case> }

            {pageType === "referee" 
            ?   <div>
                    <input
                        value={playersScore[1]}
                        className={styles.score}
                        type="text"
                        autoComplete="off"
                        maxLength={1}
                        pattern = "^0*[0-3]\d*$"
                        onBlur={(e) => (e.target.value === "" || Number(e.target.value) > 0) && handleSetScore('player2Score', Number(e.target.value))}
                        onChange={(e) => setPlayersScore(prev => [prev[0], e.target.value])}
                    />
                </div>
            : null}
            
            {gameInfo.participant2 === null 
            ? <NewElementBtn classNameProps={classNames({[styles.add_empty_participant_admin]: pageType === 'admin'},
                                                        {[styles.add_empty_participant_referee]: pageType === "referee"})}
             onClick={() => openModal('participant2')}/>
            : <Error textChildren={errorMessage} isError={error} borderRadius = {19} left1 = {85}>{participant2}</Error>}

        </div>
    )
}

export default GameInfo
