import React, {useState} from 'react';
import { CheckBox } from 'shared';

import styles from './GameRegistration.module.scss'

function GameRegistration() {
   
    const [checkboxValue, setCheckboxValue] = useState(false);
    const [gameStart, setGameStart] = useState('с');
    const [gameFinish, setGameFinish] = useState('до');

    const handleChange=(data: boolean) => {
        if (data) {
            setCheckboxValue(true)
        } else {
            setGameStart('16:30')
            setGameFinish('21:00')
        }
    }


    return (
        <div >
            <CheckBox onClick={() => handleChange(checkboxValue)}>Буду с 16:30 до 21:00</CheckBox>
            <div className={styles.input__wrapper}>

                <div className={styles.input_bx}>
                    <input 
                        type="text"
                        autoComplete="off"
                        value={gameStart}/>
                    <span>c 16:30</span>
                </div>

                <div className={styles.input_bx}>
                    <input 
                        type="text"
                        autoComplete="off"
                        value={gameFinish}/>
                    <span>до 21:00</span>
                </div>

            </div>
        </div>
    )
}

export default GameRegistration
