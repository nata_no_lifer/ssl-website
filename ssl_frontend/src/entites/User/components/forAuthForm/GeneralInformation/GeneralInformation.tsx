import { NewTextInput } from 'shared';
import addImage from 'assets/icons/functional_icons/addPhoto.svg';
import styles from './GeneralInformation.module.scss';


function GeneralInformation(props: any) {
    const {image, setImage} = props.image;
    const {surname, setSurname, surnameError, setSurnameError} = props.surname;
    const {name, setName, nameError, setNameError} = props.name;
    const {fathername, setFathername, fathernameError, setFathernameError} = props.fathername;
    const {hsePass, setHsePass} = props.hsePass;
    const {conditions, setConditions, conditionsError, setConditionsError} = props.conditions;

    return (
        <>
            <h1 className={styles.headline}>Регистрация</h1>

            <div className={styles.image}>
                <div className={styles.title}>Фотография профиля</div>
                <div className={styles.image_input}>
                    <input
                        type="file"
                        className={styles.inputFile}
                        onChange={(e) => {
                            const file = e.target.files;
                            if (file) {
                                setImage(file[0]);
                            }
                        }} 
                    />

                    <img src={image || addImage} alt="add_image" />
                </div>
            </div>

            <div className={styles.inputs}>
                <NewTextInput 
                    name='surname'
                    label="Фамилия"
                    startValue={surname}
                    setCurrentValue={setSurname}
                    error={surnameError}
                    setError={setSurnameError}
                />
                <NewTextInput
                    name='name'
                    label="Имя"
                    startValue={name}
                    setCurrentValue={setName}
                    error={nameError}
                    setError={setNameError}
                />
                <NewTextInput
                    name='fathername'
                    label="Отчество"
                    startValue={fathername}
                    setCurrentValue={setFathername}
                    error={fathernameError}
                    setError={setFathernameError}
                />
            </div>
            
            <div className={styles.point}>
                <input type="checkbox" className={styles.box} checked={hsePass} onChange={(e) => {setHsePass(e.target.checked)}} />
                <div className={styles.text}>
                    Мне нужен пропуск в ВШЭ
                </div>
            </div>

            <div className={styles.point}>
                <input type="checkbox" className={styles.box} checked={conditions} onChange={(e) => {setConditions(e.target.checked)}} />
                <div className={styles.text}>
                    Я принимаю “условия использования” и “политику конфеденциальности”
                </div>
            </div>
        </>
    );
}


export default GeneralInformation;