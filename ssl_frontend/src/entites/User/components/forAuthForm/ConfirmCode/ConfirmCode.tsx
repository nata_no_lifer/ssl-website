import { NewTextInput } from 'shared';
import styles from './ConfirmCode.module.scss';


function ConfirmCode(props: any) {
    const {code, setCode, codeError, setCodeError} = props.code;
    const resend = props.resend;

    return (
        <>
            <h1 className={styles.headline}>Код подтверждения</h1>
            <div className={styles.subtitle}>
                Проверьте почту, мы отправили код<br />для подтверждения
            </div>
            
            <div className={styles.inputs}>
                <NewTextInput
                    name='code'
                    label="Код подтверждения"
                    startValue={code}
                    setCurrentValue={setCode}
                    error={codeError}
                    setError={setCodeError}
                />
            </div>
            
            
            <div className={styles.resend}>
                <div>Не пришел код?</div>
                <div><div onClick={() => resend()}>Отправить код снова</div></div>
            </div>
        </>
    );
}

export default ConfirmCode;