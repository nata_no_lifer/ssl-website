import styles from './Success.module.scss';

function Success(props: any) {
    const email = props.email;

    return (
        <>
            <div className={styles.image}>
            </div>

            <div className={styles.headline}>
                Ваша заявка на участие
                <br />
                в клубе принята
            </div>

            <div className={styles.subtitle}>
                Ожидайте ответ в течение трёх дней.
                <br />
                Результат рассмотрения заявки придет
                <br />
                на {email}
            </div>
        </>
    );
}


export default Success;