import { NewTextInput } from 'shared';
import styles from './ProfileInformation.module.scss';

function ProfileInformation(props: any) {
    const {email, setEmail, emailError, setEmailError} = props.email;
    const {telegram, setTelegram, telegramError, setTelegramError} = props.telegram;
    const {password, setPassword, passwordError, setPasswordError} = props.password;
    const {repeatedPassword, setRepeatedPassword, repeatedPasswordError, setRepeatedPasswordError} = props.repeatedPassword;


    return (
        <div className={styles.inputs}>
            <h1 className={styles.headline}>Регистрация</h1>
            <NewTextInput
                name='email'
                label="Email"
                startValue={email}
                setCurrentValue={setEmail}
                error={emailError}
                setError={setEmailError}
            />
            <NewTextInput
                name='telegram'
                label="Telegram"
                startValue={telegram}
                setCurrentValue={setTelegram}
                error={telegramError}
                setError={setTelegramError}
            />
            <NewTextInput
                name='password'
                label="Пароль"
                startValue={password}
                setCurrentValue={setPassword}
                error={passwordError}
                setError={setPasswordError}
            />
            <NewTextInput
                name='repeatedPassword'
                label="Пароль повторно"
                startValue={repeatedPassword}
                setCurrentValue={setRepeatedPassword}
                error={repeatedPasswordError}
                setError={setRepeatedPasswordError}
            />
        </div>
    );
}

export default ProfileInformation;