import { Role } from "shared/types/user";
import { StatusType } from "../lib/Status2Level";

export default interface Unregister {
    id: number,
    first_name: string,
    last_name: string,
    father_name: string,
    telegram: string,
    image: string,
    email: string,
    hse_pass: boolean,
    is_accepted: boolean,
    is_verified_email: boolean,
    role: Role,
    game_status: StatusType
} 
