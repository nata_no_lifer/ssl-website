import { ActionReducerMapBuilder, createSlice } from "@reduxjs/toolkit";
import { UserProps } from "shared/types/user";
import {TStatusType} from 'shared'
import GameRegister from "../types/Unregister";
import status2Level from "../lib/Status2Level";
import { tablesFetched } from "entites/TableList/model/tableSlice";
import { GameType } from "shared/types/table";
import { ParticipantProps } from "shared/types";
import Unregister from "../types/Unregister";

const initialState: State = {
    userMap: {},
    usersLoadingStatus: 'idle'
}

interface State {
    userMap: {[id: number]: ParticipantProps}
    usersLoadingStatus: TStatusType
}


const usersSlice = createSlice({
    name: 'users',
    initialState,
    reducers: {
        usersFetching: (state: State) => {state.usersLoadingStatus = 'loading'},

        usersFetched: (state: State, action: {payload: Array<Unregister>}) => {
            state.usersLoadingStatus = 'idle';
            state.userMap = Object.fromEntries(action.payload.map((unregister: Unregister): [number, ParticipantProps] => ([unregister.id, {
                id: unregister.id,
                firstName: unregister.first_name,
                lastName: unregister.last_name,
                level: status2Level(unregister.game_status),
                img: unregister.image,
                arrivalTime: "",
                departureTime: "",
                countConflictsGames: 0,
                countNegotiationsGames: 0,
                role: unregister.role,
                allTime: false
            }])))
        },

        usersFetchingError: (state: State) => {
            state.usersLoadingStatus = 'error';
        },

        usersDeleted: (state: State, action: {payload: number}) => {
            delete state.userMap[action.payload];
        }
        
    }
});

const {actions, reducer} = usersSlice;

export default reducer;
export const {
    usersFetching,
    usersFetched,
    usersFetchingError,
    usersDeleted
} = actions;