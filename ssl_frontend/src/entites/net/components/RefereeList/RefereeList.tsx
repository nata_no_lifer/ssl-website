import { Referee } from 'shared';
import { TableProps } from 'shared/types';
import styles from './RefereeList.module.scss';
import { deleteReferee } from 'app/actions';
import { useDispatch } from 'react-redux';
import useHttp from 'app/hooks/http';
import { RefereeProps } from 'shared/types/referee';
import Error from 'shared/ui/Error/Error';
import { useMemo } from 'react';


interface RefereeListProps {
    isEdited?: boolean
    refereeList?: Array<RefereeProps>
    table: TableProps
    openModal: () => void
    statusType: 'show' | 'hide'
}

function RefereeList(refereeProps: RefereeListProps) {
    const { refereeList, openModal, statusType = 'hide', table, isEdited = true } = refereeProps;

    const dispatch = useDispatch();
    const {request} = useHttp();

    const refereeObj = useMemo(() => {
        let refereeObj: {[registerID: number]: number} = {};
        refereeList && refereeList.forEach((referee: RefereeProps)=> {
            if (typeof refereeObj[referee.id] === "undefined") {
                refereeObj[referee.id] = referee.refereeId
            }
        });

        return refereeObj
    }, [refereeList]);

    return (
        <div className={styles.refereeList__wrapper}>
                {refereeList?.map((referee: RefereeProps, i: number) =>
                    <span key={i}>
                        <Error 
                            textChildren='Арбитр уже добавлен'
                            zindex = {2 * (refereeList!.length - i) + 1}
                            isError={referee.refereeId !== refereeObj[referee.id]}
                        >
                            <Referee
                                first_name={referee.firstName}
                                last_name={referee.lastName}
                                // @ts-ignore
                                handleDelete={() => dispatch(deleteReferee(request, table, referee))}
                            />
                        </Error>
                    </span>
                )}

                    {
                        isEdited && <div className={`${styles.referee_skeleton__wrapper} ${styles[statusType]}`} onClick={openModal}>
                                        <button className={styles.btn__skeleton_add} >+</button>
                                    </div>
                    }

        </div>
    )
}

export default RefereeList
