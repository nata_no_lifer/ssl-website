import { Case } from 'shared';

import styles from './CaseList.module.scss';
import { CaseProps } from 'shared/types';

interface CasesProps {
    casesList: Array<CaseProps>
    className?: string
}

function CaseList(props: CasesProps) {

    const { casesList, className } = props;
    return (
        <div className={`${className} ${styles.caselist_wrapper_main}`}>
            <div className={styles.caseslist__wrapper}>
                {/* el: UsersProps, id: number */}
                {casesList.map((el: CaseProps ) => {
                    return (
                        <div className={styles.caseslist__wrapper}>
                            <Case btn_close='hide' className={styles.case_num} />
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default CaseList
