import React from 'react';
import { Btn, Title, ModalWrapper, Overlay } from 'shared';

import styles from './SendNetModalSuccess.module.scss';

import success from 'assets/icons/functional_icons/Success.svg';

interface Props {
    isSendNet: boolean
    handleClose: () => void
}

// TODO: не плодить 3 модалки, а сделать отображение через состояния

const SendNetModalSuccess = (props: Props) => {
    const { isSendNet, handleClose } = props  

    const PopupTitle = 'Сетка успешно отправлена';
    return (
        <>
            <Overlay isActive= {isSendNet} onClick={handleClose} className={styles.overlay}/>
            
            <ModalWrapper isModalOpen={isSendNet}
                     closeModal={handleClose}
                     showCloseBtn={true}
                     wrapperClass={styles.wrapperModal}>
                <img src={success} className={styles.img_success} alt='success icon'></img>
                <div className={styles.form__wrapper}>
                    <Title type="h3" margin='mb_24' className={styles.left}>{PopupTitle}</Title>

                </div>
                <div className={styles.btn__wrapper}>
                    <Btn 
                        onClick={handleClose}
                        type='button'
                    >
                      Закрыть
                    </Btn>
                </div>

            </ModalWrapper>
        </>

    )
}

export default SendNetModalSuccess