import { Btn, Title, Text, ModalWrapper, Overlay, askAgain } from 'shared';

import styles from './SendNetModal.module.scss';

interface Props {
    isSendNet: boolean
    handleSend?: () => void
    handleClose: () => void
}

const SendNetModal = (props: Props) => {
    const {isSendNet,  handleSend, handleClose} = props  

    const PopupTitle = 'Отправка сетки';

    const handleCheckBoxChange = (e: any) => {
        if (e.target.checked) {
            localStorage.setItem(askAgain, 'no'); 
        } else {
            localStorage.setItem(askAgain, 'yes');
        }

    }

    return (
        <>
            <Overlay isActive= {isSendNet} onClick={handleClose} className={styles.overlay}/>
            
            <ModalWrapper isModalOpen={isSendNet}
                     closeModal={handleClose}
                     showCloseBtn={true}
                     wrapperClass={styles.wrapperModal}>
                <div className={styles.form__wrapper}>
                    <Title type="h2" margin='mb_24' className={styles.left}>{PopupTitle}</Title>
                    <Text type='paragraph' className={styles.left}>Участникам отправятся все столы<br />Точно хотите отправить?</Text>
                    <div className={styles.checkbox__wrapper}>
                        <input type='checkbox' className={styles.left} onClick={e => handleCheckBoxChange(e)}/>
                        <label htmlFor="">Не показывать больше</label>
                    </div>

                </div>
                <div className={styles.btn__wrapper}>
                    <Btn 
                        onClick={handleClose}
                        type='button'
                    >
                      Назад
                    </Btn>
                    <Btn
                        onClick={handleSend}
                        btnType='outlined' 
                        type='button'
                    >Отправить
                    </Btn>
                </div>

            </ModalWrapper>
        </>
    )
}

export default SendNetModal