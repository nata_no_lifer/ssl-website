import React from 'react';
import { Btn, Title, Text, ModalWrapper, Overlay } from 'shared';

import styles from './SendNetModalError.module.scss';
import error from '../../../../assets/icons/functional_icons/error.svg';


interface Props {
    isSendNet: boolean
    handleSend?: () => void
    handleClose: () => void
    errorMessage: string | null
}

const SendNetModal = (props: Props) => {
    const { isSendNet,  handleSend, handleClose, errorMessage } = props  

    const PopupTitle = 'Сетка не отправлена';
    return (
        <>
            <Overlay isActive= {isSendNet} onClick={handleClose} className={styles.overlay}/>
            
            <ModalWrapper isModalOpen={isSendNet}
                     closeModal={handleClose}
                     showCloseBtn={true}
                     wrapperClass={styles.wrapperModal}>
                <img src={error} alt="send net error" className={styles.img_success} />
                <div className={styles.form__wrapper}>
                    <Title type="h3" margin='mb_24' className={styles.left}>{PopupTitle}</Title>
                    {errorMessage === null
                    ? <Text type='paragraph' className={styles.left}>Попробуйте еще раз позже</Text>
                    : <div className="">{errorMessage}</div>}
                    

                </div>
                <div className={styles.btn__wrapper}>
                    <Btn 
                        onClick={handleClose}
                        type='button'
                        className={styles.btn_error}

                    >
                      Закрыть
                    </Btn>
                    <Btn
                        onClick={handleSend}
                        btnType='outlined' 
                        type='button'
                        className={styles.btn_error}
                    >Отправить повторно
                    </Btn>
                </div>

            </ModalWrapper>
        </>

    )
}

export default SendNetModal