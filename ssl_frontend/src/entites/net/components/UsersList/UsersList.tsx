import {Participant} from 'shared';
import classNames from 'classnames';
import styles from './UsersList.module.scss';
import { ParticipantProps } from 'shared/types';
import { setStatus } from 'app/actions';
import { RootState } from 'app/store';
import { useDispatch } from 'react-redux';
import useHttp from 'app/hooks/http';
import { CaseType } from 'shared/types/case';
import sortUsersByGameType from '../../../../features/SelectUser/lib/sortUsersByGameType';

interface UsersProps {
    usersList: Array<ParticipantProps>
    className?: string
    pageType: "referee" | "admin"
    gameType: CaseType
}

function UsersList(usersProps: UsersProps) {
    const { usersList, className, pageType, gameType } = usersProps;
    
    const dispatch = useDispatch();
    const {request} = useHttp();

    usersList.sort(sortUsersByGameType(gameType));
    return (

        <div className={`${className} ${styles.userlist_wrapper_main}`}>
            <div className={styles.userslist__wrapper}>
                {/* el: UsersProps, id: number */}
                {usersList.map((el: ParticipantProps, id: number) => {
                    return (
                        <span
                            key={id}
                            className={classNames(
                                styles.span,
                                {[styles.transparent]: el.countConflictsGames > 0 && gameType === 'conflicts' ||  el.countNegotiationsGames > 0 && gameType === 'negotiations'})}
                        >

                            <Participant
                                //@ts-ignore
                                onStatusChange={(status: RootState) => dispatch(setStatus(request, el, status))}
                                pageType={pageType}
                                participantData={el}
                            />
                        </span>
                    )   
                })}
            </div>
        </div>
    )
}

export default UsersList
