import { UsersList } from "./net/components/UsersList";
import GameInfo from "./Game/components/GameInfo/GameInfo";
// import RefereeGameInfo from "./Game/components/RefereeGameInfo/RefereeGameInfo";
import ProfileInformation from "./User/components/forAuthForm/ProfileInformation/ProfileInformation";
import GeneralInformation from "./User/components/forAuthForm/GeneralInformation/GeneralInformation";
import ConfirmCode from "./User/components/forAuthForm/ConfirmCode/ConfirmCode";
import Success from "./User/components/forAuthForm/Success/Success";

export {UsersList,
        GameInfo,
        GeneralInformation,
        ProfileInformation,
        ConfirmCode, 
        Success
};