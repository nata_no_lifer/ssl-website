import{StatusType} from "../lib/Status2Level";

export default interface Profile {
    id: number,
    first_name: string,
    last_name: string,
    father_name:string,
    telegram: string,
    image: string,
    email: string,
    hse_pass: boolean,
    is_accepted: boolean,
    is_verified_email: boolean,
    role: 'user' | 'student' | 'arbitrator' | 'admin',
    game_status: StatusType
}