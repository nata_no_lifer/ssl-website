import GameRegister from "./GameRegister"
import Profile from "./Profile"

export default interface Player {
    user: Profile
    game_registration: GameRegister
}