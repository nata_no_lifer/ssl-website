import Profile from "./Profile";

export default interface GameRegister {
    id: number,
    date: string,
    begin_time: string,
    finish_time: string,
    player: Profile,
    attendance: "Не пришел" | "Опоздал",
    count_games: number
    allTime: boolean
}
