import { ActionReducerMapBuilder, createSlice } from "@reduxjs/toolkit";
import { ParticipantProps } from "shared/types/participant";
import {TStatusType} from 'shared'
import GameRegister from "../types/GameRegister";
import status2Level from "../lib/Status2Level";
import { tablesFetched } from "entites/TableList/model/tableSlice";
import { GameType } from "shared/types/table";
import TableAPI from "entites/TableList/types/TableAPI";
import GameAPI from "entites/TableList/types/GameAPI";

const initialState: State = {
    participantMap: {},
    participantsLoadingStatus: 'idle'
}

interface State {
    participantMap: {[id: number]: ParticipantProps}
    participantsLoadingStatus: TStatusType
}


const participantsSlice = createSlice({
    name: 'participants',
    initialState,
    reducers: {
        participantsFetching: (state: State) => {state.participantsLoadingStatus = 'loading'},

        participantsFetched: (state: State, action: {payload: Array<GameRegister>}) => {
            state.participantsLoadingStatus = 'idle';
            state.participantMap = Object.fromEntries(action.payload.map((register: GameRegister): [number, ParticipantProps] => ([register.id, {
                id: register.id,
                firstName: register.player.first_name,
                lastName: register.player.last_name,
                level: status2Level(register.player.game_status),
                img: register.player.image,
                arrivalTime: register.begin_time.slice(0,5),
                departureTime: register.finish_time.slice(0,5),
                countConflictsGames: 0,
                countNegotiationsGames: 0,
                allTime: register.allTime,
                attendance: register.attendance,
                role: register.player.role,
            }])))
        },

        participantsFetchingError: (state: State) => {
            state.participantsLoadingStatus = 'error';
        },

        // change participant game counter
        participantsChangeGames: (state: State, action: {payload: {id: number, type: GameType, delta: number, score: number}}) => {
            if (state.participantMap[action.payload.id]) {
                let participant: ParticipantProps = state.participantMap[action.payload.id]!
                let counter: 'countConflictsGames' | 'countNegotiationsGames' = action.payload.type === 'conflicts' 
                                                                                ? 'countConflictsGames' 
                                                                                : 'countNegotiationsGames'
                state.participantMap = {
                    ...state.participantMap,
                    [action.payload.id]: {
                        ...participant,
                        [counter]: participant[counter]! + action.payload.delta
                    }
                }
            } else {
                console.log('invalid participant')
            }
        },

        // add new register participant
        participantsCreated: (state: State, action: {payload: GameRegister}) => {
            console.log(action)
            state.participantMap = {
                ...state.participantMap,
                [action.payload.id]: {
                    id: action.payload.id,
                    firstName: action.payload.player.first_name,
                    lastName: action.payload.player.last_name,
                    level: status2Level(action.payload.player.game_status),
                    img: action.payload.player.image,
                    arrivalTime: action.payload.begin_time.slice(0,5),
                    departureTime: action.payload.finish_time.slice(0,5),
                    countConflictsGames: 0,
                    countNegotiationsGames: 0,
                    role: action.payload.player.role,
                    allTime: action.payload.allTime
                }
            }
        },

        participantsSetState: (state: State, action: {payload: {id: number, status: "Не пришел" | "Опоздал"}}) => {
            if (state.participantMap[action.payload.id]) {
                let participant: ParticipantProps = state.participantMap[action.payload.id]
                state.participantMap = {
                    ...state.participantMap,
                    [action.payload.id]: {
                        ...participant,
                        attendance: action.payload.status
                   }
                }
            } else {
                console.log('participant status error')
            }
        }
        
    },
    extraReducers: (builder: ActionReducerMapBuilder<State>) => {
        builder.addCase(tablesFetched, (state: State, action: { payload: TableAPI[]}) => {
            action.payload.forEach((table: TableAPI) => {
                table.negotiations.forEach((game: GameAPI) => {
                    if (game.player1) {
                        state.participantMap[game.player1.game_registration.id].countNegotiationsGames += 1
                    }
                    if (game.player2) {
                        state.participantMap[game.player2.game_registration.id].countNegotiationsGames += 1
                    }
                })
                table.conflicts.forEach((game: GameAPI) => {
                    if (game.player1) {
                        state.participantMap[game.player1.game_registration.id].countConflictsGames += 1
                    }
                    if (game.player2) {
                        state.participantMap[game.player2.game_registration.id].countConflictsGames += 1
                    }
                })
            })
        })
    }
});

const {actions, reducer} = participantsSlice;

export default reducer;
export const {
    participantsFetching,
    participantsFetched,
    participantsFetchingError,
    participantsCreated,
    participantsChangeGames,
    participantsSetState
} = actions;