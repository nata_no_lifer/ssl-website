import Case from "entites/Case/types/Case"
import Player from "entites/Participant/types/Player"

export default interface GameAPI {
    id: number
    case: Case
    player1: Player
    player1_score: number
    player2: Player
    player2_score: number
    table: number
}