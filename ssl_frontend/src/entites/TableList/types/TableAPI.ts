import GameAPI from "./GameAPI"
import { RefereeAPI } from "./RefereeAPI"

export default interface TableAPI {
    conflicts: GameAPI[]
    negotiations: GameAPI[]  
    referees: RefereeAPI[]
    table: {
        date: string
        id: number
        number: string // table name
    }
}