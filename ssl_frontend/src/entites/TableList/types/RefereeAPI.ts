import Player from "entites/Participant/types/Player"

export interface RefereeAPI {
    date: string
    id: number
    referee: Player
    table: number[]
}

