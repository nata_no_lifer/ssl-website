import React from 'react';
import { Btn, Title, Text, ModalWrapper, Overlay } from 'shared';

import styles from './DeleteTable.module.scss';

interface Props {
    isDeleteTable: boolean
    handleDelete?: () => void
    handleClose: () => void
}

const DeleteTable = (props: Props) => {
    const { isDeleteTable, handleDelete, handleClose } = props  

    const PopupTitle = 'Удаление стола';
    // TODO: Отцентровать по началу см в фигме
    return (
        <>
            <Overlay isActive= {isDeleteTable} onClick={handleClose}/>
            <ModalWrapper isModalOpen={isDeleteTable}
                     closeModal={handleClose}
                     showCloseBtn={true}
                     wrapperClass={styles.wrapperModal}>
                <div className={styles.form__wrapper}>
                    <Title type="h2" margin='mb_24' className={styles.left}>{PopupTitle}</Title>
                    <Text type='paragraph' className={styles.left}>Точно хотите удалить? <br />Данные не сохранятся</Text>
                </div>
                <div className={styles.btn__wrapper}>
                    <Btn 
                        onClick={handleClose}
                        type='button'
                    >
                      Назад
                    </Btn>
                    <Btn
                        onClick={handleDelete}
                        btnType='outlined' 
                        type='button'
                    >Удалить
                    </Btn>
                </div>

            </ModalWrapper>
        </>

    )
}

export default DeleteTable