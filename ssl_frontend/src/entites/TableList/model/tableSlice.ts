import { createSlice } from "@reduxjs/toolkit";
import { TStatusType } from 'shared'
import { GameType, TableBD } from "shared/types/table";
import { GameInfoBD, IGameInfo } from "shared/types/game";
import TableAPI from "../types/TableAPI";
import { RefereeAPI } from "../types/RefereeAPI";
import GameAPI from "../types/GameAPI";

interface State {
    tables: Array<TableBD>
    tablesLoadingStatus: TStatusType
}

const initialState: State = {
    tables: new Array<TableBD>(),
    tablesLoadingStatus: 'idle'
}

const tableSlice = createSlice({
    name: 'tables',
    initialState,
    reducers: {
        tablesFetching: (state: State) => {state.tablesLoadingStatus = 'loading'},

        tablesFetched: (state: State, action: { payload: TableAPI[]}) => {
            state.tablesLoadingStatus = 'idle';
            state.tables = action.payload.map((table: TableAPI): TableBD => ({
                id: table.table.id,
                tableName: table.table.number,
                referees: Object.fromEntries(table.referees.map((referee: RefereeAPI): [number, number] => [referee.id, referee.referee.game_registration.id])),
                negotiations: table.negotiations.map((game: GameAPI): GameInfoBD => ({
                    id: game.id,
                    case: game.case && game.case.id,
                    participant1: game.player1 && game.player1.game_registration.id,
                    participant2: game.player2 && game.player2.game_registration.id,
                    player1Score: game.player1_score ? game.player1_score : 0,
                    player2Score: game.player2_score ? game.player2_score : 0,
                })),
                conflicts: table.conflicts.map((game: GameAPI): GameInfoBD => ({
                    id: game.id,
                    case: game.case && game.case.id,
                    participant1: game.player1 && game.player1.game_registration.id,
                    participant2: game.player2 && game.player2.game_registration.id,
                    player1Score: game.player1_score ? game.player1_score : 0,
                    player2Score: game.player2_score ? game.player2_score : 0,
                }))
            }))
        },

        tablesFetchingError: (state: State) => {
            state.tablesLoadingStatus = 'error';
        },

        tablesCreated: (state: State, action: {payload: {id: number, name: string}}) => {
            state.tables = [
                ...state.tables, 
                {
                    id: action.payload.id,
                    tableName: action.payload.name,
                    referees: [],
                    negotiations: [],
                    conflicts: [],
                }
            ]
        },

        tablesDeleted: (state: State, action: {payload: {id: number}}) => {
            console.log(action.payload.id)
            state.tables = state.tables.filter((table: TableBD): boolean => table.id !== action.payload.id)
        },

        tablesAddReferee: (state: State, action: {payload: {id: number, register: number, referee: number }}) => {
            state.tables = state.tables.map((table: TableBD): TableBD => {
                if (table.id !== action.payload.id) {
                    return table
                }

                return {
                    ...table,
                    referees: {
                        ...table.referees, 
                        [action.payload.referee]: action.payload.register 
                    }
                }
            })
        },


        tablesDeleteReferee: (state: State, action: {payload: {id: number, referee: number}}) => {
            state.tables = state.tables.map((table: TableBD): TableBD => {
                if (table.id !== action.payload.id) {
                    return table
                }

                let referees = {...table.referees}

                delete referees[action.payload.referee]
                return {
                    ...table,
                    referees: referees
                }
            })
        },

        tablesChangeName: (state: State, action: {payload: {id: number, name: string}}) => {
            state.tables = state.tables.map((table: TableBD): TableBD => {
                if (table.id === action.payload.id) {
                    table.tableName = action.payload.name
                }

                return table
            })
        },

        tablesAddGame: (state: State, action: {payload: {id: number, type: GameType, gameID: number}}) => {
            state.tables = state.tables.map((table: TableBD): TableBD => {
                if (table.id !== action.payload.id) {
                    return table
                }
                console.log(action.payload.type)
                return {
                    ...table,
                    [action.payload.type]: [
                        ...table[action.payload.type], 
                        {
                            id: action.payload.gameID,
                            participant1: null,
                            participant2: null,
                            case: null,
                        }
                    ]
                }
            })
        },

        tablesUpdateGame: (state: State, action: {payload: {id: number, type: GameType, game: GameInfoBD}}) => {
            state.tables = state.tables.map((table: TableBD): TableBD => {
                if (table.id !== action.payload.id) {
                    return table
                }

                return {
                    ...table,
                    [action.payload.type]: table[action.payload.type].map((game: GameInfoBD): GameInfoBD => {
                        if (game.id !== action.payload.game.id) {
                            return game
                        }

                        return {
                            ...game, 
                            ...action.payload.game
                        }
                    })
                }
            })
        }
    }
});

const {actions, reducer} = tableSlice;

export default reducer;
export const {
    tablesChangeName,
    tablesFetching,
    tablesFetched,
    tablesFetchingError,
    tablesAddGame,
    tablesAddReferee,
    tablesUpdateGame,
    tablesCreated,
    tablesDeleted,
    tablesDeleteReferee,
} = actions;
