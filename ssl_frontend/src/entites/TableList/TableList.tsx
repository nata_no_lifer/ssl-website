import { useState } from 'react';
import { NewElementBtn } from 'shared';
import styles from './TableList.module.scss';
import close_icon from 'assets/icons/functional_icons/close.png';
import classNames from 'classnames';
import DeleteTable from './components/DeleteTable/DeleteTable';
import { TableProps } from 'shared/types';
import { useDispatch } from 'react-redux';
import useHttp from 'app/hooks/http';
import { addTable, deleteTable } from 'app/actions';

interface Props {
    tableList: Array<TableProps>
    selectedTableId: number
    setSelectedTableId: React.Dispatch<React.SetStateAction<number>>
}

function TableList(props: Props) {
    const { tableList, selectedTableId, setSelectedTableId } = props;
    const [modalState, setModalState] = useState<number | null>(null);

    const dispatch = useDispatch();
    const {request} = useHttp();

    return (
        <div className={styles.tableList__wrapper}>
            {tableList.map((el: TableProps, id: number) => {
                return (
                    <div key={id} className={classNames(styles.table__wrapper, {
                        [styles.table__wrapper_selected] : id === selectedTableId})}
                     onClick = {() => setSelectedTableId(id)}>
                        <p className={`${styles.name}`}>{el.tableName !== "" ? el.tableName : `Стол ${id + 1}`}</p>
                        <button onClick = {() => setModalState(id)} className={styles.close_icon}>
                          <img src={close_icon} alt="" />
                        </button>
                    </div>
                )
            })}
            <DeleteTable 
                handleClose={() => setModalState(null)}
                handleDelete={() => {
                    //@ts-ignore
                    dispatch(deleteTable(request, tableList[modalState]))
                    console.log(selectedTableId, modalState)
                    setSelectedTableId((old: number) => (old !== 0 && old >= modalState!) ? old - 1 : old)
                    setModalState(null);
                }}
                isDeleteTable={modalState !== null}
            />
            {/* TODO: Вынести в отдельную функцию */}
            <NewElementBtn classNameProps={styles.new_table} onClick={() => 
                 //@ts-ignore
                 dispatch(addTable(request, `table${tableList.length+1}`))
            }/>

        </div>
    )
}

export default TableList