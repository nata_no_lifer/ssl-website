import { ParticipantProps } from "shared/types";
import { CaseType } from "shared/types/case";

export default function emphasisCondition (gameType: CaseType) {
    return (participant: ParticipantProps) : boolean => {
        if (gameType === 'conflicts') {
            return participant.countConflictsGames > 0
        } else {
            return participant.countNegotiationsGames > 0
        }
    }
}