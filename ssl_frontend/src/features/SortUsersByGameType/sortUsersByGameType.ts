import { ParticipantProps } from "shared/types";

export default function sortUsersByGameType (property: 'conflicts' | 'negotiations') {
    return (a: ParticipantProps, b: ParticipantProps) : number => {
        if (property === 'conflicts') {
            if (a.countConflictsGames < b.countConflictsGames) {
                return -1;
              }
            if (a.countConflictsGames > b.countConflictsGames) {
                 return 1;
            }
        }
        if (property === 'negotiations') {
            if (a.countNegotiationsGames < b.countNegotiationsGames) {
                return -1;
              }
            if (a.countNegotiationsGames > b.countNegotiationsGames) {
                 return 1;
            }
        }
        return 0;
    }

}