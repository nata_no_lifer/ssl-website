import { FormEvent, useState } from 'react';
import { Navigate } from "react-router-dom";
import Cookies from 'js-cookie';
import { NewTextInput, Btn, routes } from 'shared';
import { url } from 'shared';
import styles from './NewLoginForm.module.scss';


function NewLoginForm() {
    const [isAuth, setIsAuth] = useState(Cookies.get('token') ? true : false);

    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');

    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState('');

    
    function RedirectHandler() {
        if (isAuth) {
            return <Navigate to={routes.mainPage} replace={true} />
        } else {
            return <></>
        }
    }


    function submit(event: FormEvent) {
        event.preventDefault();
        loginUser();
    }


    const loginUser = async () => {
        const profile = new FormData();
        profile.append("email", email);
        profile.append("password", password);

        const response = await fetch(`${url}/users/auth/login/` ,{
            method: 'POST',
            body: profile
        });

        const json = await response.json();

        if (response.ok) {
            setIsAuth(true);
            Cookies.set('token', json.tokens.access);
            Cookies.set('id', json.tokens.id);
        }
    }


    function LoginScreen() {
        return (
            <div>
                <h1 className={styles.headline}>Вход в аккаунт</h1>
                <div className={styles.auth_container}>
                    <NewTextInput
                        name='email'
                        label="Email"
                        startValue={email}
                        setCurrentValue={setEmail}
                        error={emailError}
                        setError={setEmailError}
                    />
                    <NewTextInput
                        name='password'
                        label="Пароль"
                        startValue={password}
                        setCurrentValue={setPassword}
                        error={passwordError}
                        setError={setPasswordError}
                    />
                </div>
            </div>
        )
    }


    return (
        <>
            <RedirectHandler />
            <form onSubmit={submit}>
                {LoginScreen()}
                <Btn type='submit'>Войти</Btn>
            </form>
        </>
    )
}


export default NewLoginForm;