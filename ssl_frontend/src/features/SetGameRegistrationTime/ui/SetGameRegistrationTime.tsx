import React from 'react';
import { Btn, Title } from 'shared';
import { GameRegistration } from 'entites/Game/components/GameRegistration';

import styles from './SetGameRegistrationTime.module.scss';

function SetGameRegistrationTime() {

    const FormTitle = 'Запись на игру';

    return (

        <form>
            <div className={styles.form__wrapper}>
                <Title type="h2" margin='mb_24'>{FormTitle}</Title>
                <GameRegistration/>
            </div>

            <Btn type='submit'>Записаться</Btn>
        </form>
    )
}

export default SetGameRegistrationTime



