import { useState } from "react";
import { NewTextInput } from "shared";
import classNames from "classnames";
import styles from './ModalPassword.module.scss';



function ModalPassword(props: any) {
    const [step, setStep] = useState(1);

    const [oldPassword, setOldPassword] = useState(''),
          [newPassword, setNewPassword] = useState(''),
          [repeatedNewPassword, setRepeatedNewPassword] = useState('');


    return (
        <>
            <div className={classNames(styles.step, step === 1 ? styles.step_active : '')}>
                <NewTextInput
                    name='oldPassword'
                    label="Старый пароль"
                    startValue={oldPassword}
                    setCurrentValue={setOldPassword}
                    error={null}
                    setError={null}
                />

                <div className={styles.button} onClick={() => setStep(2)}>Продолжить</div>
            </div>


            <div className={classNames(styles.step, step === 2 ? styles.step_active : '')}>
                <div className={styles.input_container}>
                    <NewTextInput
                        name='newPassword'
                        label="Новый пароль"
                        startValue={newPassword}
                        setCurrentValue={setNewPassword}
                        error={null}
                        setError={null}
                    />

                    <NewTextInput
                        name='code'
                        label="Новый пароль повторно"
                        startValue={repeatedNewPassword}
                        setCurrentValue={setRepeatedNewPassword}
                        error={null}
                        setError={null}
                    />
                </div>

                <div className={styles.button} onClick={() => props.close()}>Сохранить</div>
            </div>
        </>
    )
}

export default ModalPassword;