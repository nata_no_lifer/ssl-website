import ModalData from "./modalData/ModalData";
import ModalEmail from "./modalEmail/ModalEmail";
import ModalPassword from "./modalPassword/ModalPassword";
import ModalNextGame from "./modalNextGame/ModalNextGame";

export {
    ModalData,
    ModalEmail,
    ModalPassword,
    ModalNextGame
}