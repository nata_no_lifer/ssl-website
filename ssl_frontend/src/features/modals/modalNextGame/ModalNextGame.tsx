import { useState } from "react";
import { NewTextInput } from "shared";
import classNames from "classnames";
import styles from './ModalNextGame.module.scss';


function ModalNextGame(props: any) {

	const [start, setStart] = useState(''),
		  [end, setEnd] = useState(''),
		  [check, setCheck] = useState(false);

	const {openModal, closeModal, saveModal} = props.setsModal;


	return (
		<>
			<div className={styles.line}>
				<input type="checkbox" checked={check} onChange={() => {
					if (check) setCheck(false);
					else setCheck(true);
				}} />
				<div>Буду с 16:30 до 21:00</div>
			</div>

			<div className={classNames(styles.inputs, check ? styles.inputs_disabled : null)} >
				<NewTextInput
					name='start'
					label="С 17:30"
					startValue={start}
					setCurrentValue={setStart}
					error={null}
					setError={() => ''}
				/>

				<NewTextInput
					name='end'
					label="До 20:30"
					startValue={end}
					setCurrentValue={setEnd}
					error={null}
					setError={() => 0}
				/>
			</div>

			<div className={styles.button} onClick={() => saveModal()}>Записаться</div>
		</>
	)
}

export default ModalNextGame;