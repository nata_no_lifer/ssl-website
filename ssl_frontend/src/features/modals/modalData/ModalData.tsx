import { useState } from "react";
import { NewTextInput } from "shared";
import closeButton from 'assets/icons/functional_icons/close.png';
import styles from './ModalData.module.scss';


function ModalData(props: any) {
    const [hoveredImg, setHoveredImg] = useState(false);
    const [isImg, setIsImg] = useState(true);

    const [surname, setSurname] = useState(''),
          [name, setName] = useState(''),
          [fathername, setFathername] = useState(''),
          [telegram, setTelegram] = useState('');


    return (
        <>
            <div className={styles.image}>
                    
            </div>
        
            <div className={styles.inputs}>
                <NewTextInput
                    name='surname'
                    label="Фамилия"
                    startValue={surname}
                    setCurrentValue={setSurname}
                    error={null}
                    setError={null}
                />

                <NewTextInput
                    name='name'
                    label="Имя"
                    startValue={name}
                    setCurrentValue={setName}
                    error={null}
                    setError={null}
                />

                <NewTextInput
                    name='fathername'
                    label="Отчество"
                    startValue={fathername}
                    setCurrentValue={setFathername}
                    error={null}
                    setError={null}
                />

                <NewTextInput
                    name='telegram'
                    label="Телеграм"
                    startValue={telegram}
                    setCurrentValue={setTelegram}
                    error={null}
                    setError={null}
                />

            </div>

            <div className={styles.button} onClick={() => props.close()}>Сохранить</div>
        </>
    )
}

export default ModalData;