import { useState } from "react";
import classNames from "classnames";
import { NewTextInput } from "shared";
import styles from './ModalEmail.module.scss';


function ModalEmail(props: any) {
    const [step, setStep] = useState(1),
          [email, setEmail] = useState(''),
          [confirmCode, setConfirmCode] = useState('');


    return (
        <>
            <div className={classNames(styles.step, step === 1 ? styles.step_active : '')}>
                <NewTextInput
                    name='email'
                    label="Новая почта"
                    startValue={email}
                    setCurrentValue={setEmail}
                    error={null}
                    setError={null}
                />

                <div className={styles.button} onClick={() => setStep(2)}>Продолжить</div>
            </div>


            <div className={classNames(styles.step, step === 2 ? styles.step_active : '')}>
                <div className={styles.comment}>Проверьте почту, мы отправили код для подтверждения</div>

                <NewTextInput
                    name='code'
                    label="Код подтверждения"
                    startValue={confirmCode}
                    setCurrentValue={setConfirmCode}
                    error={null}
                    setError={null}
                />

                <div className={styles.again}>
                    <div>Не пришел код? &nbsp;</div>
                    <div className={styles.again_link}>Отправить код снова</div>
                </div>

                <div className={styles.button} onClick={() => props.close()}>Сохранить</div>
            </div>
        </>
    )
}

export default ModalEmail;