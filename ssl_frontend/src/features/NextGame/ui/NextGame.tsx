import { useState } from 'react';
import Cookies from 'js-cookie';
import { url } from 'shared';

import place from 'assets/icons/element_icons/place.png';
import calendar from 'assets/icons/element_icons/calendar.png';
import pointer from 'assets/icons/element_icons/pointer.png';
import flask from 'assets/icons/element_icons/flask.png';
import attention from 'assets/icons/functional_icons/attention.png';
import change from 'assets/icons/functional_icons/change.png';

import styles from './NextGame.module.scss';
import classNames from 'classnames';


function NextGame(props: any) {

	const {openModal, closeModal, saveModal} = props.setsModal;


	const loadGame = async () => {
		let token = `Bearer ${Cookies.get('token')}`;
		console.log(token);

        const response = await fetch(`${url}/api/classes/game_register/`, {
            method: 'GET',
            headers: {
                Authorization: token
            }
        });

        const json = await response.json();
        console.log(json);
	};


	const setContent = () => {
		switch(props.setsModal.step) {
			case 0:
				return (
					<section className={classNames(styles.next_game, styles.block_one)}>
						<div className={classNames(styles.line, styles.first_line)}>
							<div className={styles.headline}>Предстоящая игра</div>

							<div className={styles.image_set}>
								<div>
									<img src={pointer} alt="" className={styles.pointer}/>
									<img src={flask} alt="" className={styles.flask}/>
								</div>
							</div>
						</div>

						<div className={classNames(styles.line, styles.second_line)}>
							<div className={styles.data}>
								<img src={place} alt="" />
								<div>Покровский бульвар, д. 11</div>
							</div>
									
							<div className={styles.data}>
								<img src={calendar} alt="" />
								<div>8 июля с 16:30 до 21:00</div>
							</div>
						</div>

						<div className={classNames(styles.line, styles.third_line)}>
							<div className={styles.button} onClick={() => openModal()}>Записаться</div>
						</div>
					</section>
				);

			case 1:
				return (
					<section className={classNames(styles.next_game, styles.block_two)}>
						<div className={classNames(styles.line, styles.first_line)}>
							<div className={styles.headline}>
								<div className={styles.title}>Предстоящая игра</div>

								<div className={styles.change}>
					 				<img src={attention} alt="" />
					 				<div>Сетка изменилась</div>
					 			</div>
							</div>

							<div className={styles.image_set}>
								<div>
									<img src={pointer} alt="" className={styles.pointer}/>
									<img src={flask} alt="" className={styles.flask}/>
								</div>
							</div>
						</div>

						<div className={classNames(styles.line, styles.second_line)}>
							<div className={styles.data}>
								<img src={place} alt="" />
								<div>Покровский бульвар, д. 11</div>
							</div>
									
							<div className={styles.data}>
								<img src={calendar} alt="" />
								<div>8 июля с 16:30 до 21:00</div>
							</div>
						</div>

						<div className={classNames(styles.line, styles.third_line)}>
							<div className={styles.new_sign}>
					 			<div>Запись на 17:00-20:30</div>
								<img src={change} alt="" />
							</div>

							<div className={styles.cancel}>Отменить запись</div>
						</div>
					</section>
				);
			
			case 2:
				return (
					<section className={classNames(styles.next_game, styles.block_three)}>
						<div className={classNames(styles.line, styles.first_line)}>
							<div className={styles.headline}>
								<div className={styles.title}>Предстоящая игра</div>

								<div className={styles.change}>
					 				<img src={attention} alt="" />
					 				<div>Сетка изменилась</div>
					 			</div>
							</div>

							<div className={styles.button} onClick={() => 0}>Открыть сетку</div>
						</div>

						<div className={classNames(styles.line, styles.second_line)}>
							<div className={styles.data}>
								<img src={place} alt="" />
								<div>Покровский бульвар, д. 11</div>
							</div>
									
							<div className={styles.data}>
								<img src={calendar} alt="" />
								<div>8 июля с 16:30 до 21:00</div>
							</div>
						</div>

						<div className={classNames(styles.line, styles.third_line)}>
							<div className={styles.new_sign}>
					 			<div>Запись на 17:00-20:30</div>
								<img src={change} alt="" />
							</div>

							<div className={styles.cancel}>Отменить запись</div>
						</div>

						<div className={styles.fourth_line}>

						</div>
					</section>
				);
		}
	};


	return (
		<>
			{setContent()}
		</>
	)
}


export default NextGame;