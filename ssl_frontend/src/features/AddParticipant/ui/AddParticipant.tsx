import React, {useState, useEffect} from 'react';
import { ModalWrapper, Overlay, Title, Btn, Participant, BaseRequest } from 'shared';
import sortUsersByGameType from '../lib/sortUsersByGameType';
import { UsersList } from 'entites';
import find_icon from '../../../assets/icons/functional_icons/find.png';
// import {find_icon} from '../../../assets/icons/functional_icons/find.png';
import { ParticipantProps, UserProps } from 'shared/types';

import classNames from 'classnames';
import styles from './AddParticipant.module.scss';

interface Props {
    notRegisterUsersList: Array<ParticipantProps>
    // usersList: Array<ParticipantProps>

    isSelectUser: boolean
    handleClose: () => void
    handleSelect: (user: ParticipantProps) => void
    className?: string
    comparator: 'conflicts' | 'negotiations';
}


const AddParticipant = (props: Props) => {
    const [selectedUser, setSelectedUser] = useState<number | null>(null);

    const [filterData, setFilterData] = useState<Array<ParticipantProps>>([])

    useEffect(() => {
        setFilterData(props.notRegisterUsersList)
    }, [props.notRegisterUsersList])

    const handleFilter = (event: any) => {
        const searchWord = event.target.value
        const newFilter = notRegisterUsersList.filter((value: ParticipantProps) => {
            return value.firstName.toLowerCase().includes(searchWord.toLowerCase()) 
                   || value.lastName.toLowerCase().includes(searchWord.toLowerCase())
                   || (value.firstName + " "  + value.lastName).toLowerCase().includes(searchWord.toLowerCase())
                   || (value.lastName + " "  + value.firstName).toLowerCase().includes(searchWord.toLowerCase())
        });
        setSelectedUser(null)
        setFilterData(newFilter)
        setError(false);
    }


    const [error, setError] = useState<boolean>(false);


    const {notRegisterUsersList} = props;
    const { isSelectUser, handleClose, handleSelect} = props;
    return (
        <>

            <Overlay isActive= {true} onClick={handleClose} className={styles.selectcase_overlay}/>
            <ModalWrapper isModalOpen = {isSelectUser}
            //  isModalOpen={isDeleteTable}
                     closeModal={handleClose}
                     showCloseBtn={true}
                     wrapperClass={styles.wrapperModal}>
                <div>
                    <Title type="h2" className={styles.title}>Регистрация участника</Title>
                    <div className={styles.input_bx}>
                        <input type="text" 
                               placeholder='Поиск по участникам' 
                               className={styles.input} 
                               onChange={handleFilter}>
                        </input>
                        <img src={find_icon} alt="find" className={styles.img} />
                    </div>

                    {error && <div>чел ты...</div>}

                    <div className={styles.userslist__wrapper}>
                        <div className={styles.userlist_user__wrapper_main}> 
                            <div className={styles.userslist_user__wrapper}>
                            
                                {filterData.map((el: ParticipantProps, id: number ) => {
                                    return (
                                        <span onClick = {() => {
                                                setSelectedUser(id);
                                                setError(false);
                                            }}
                                            className={classNames(styles.span,
                                                {[styles.span_selected]: selectedUser === id})}>

{/* <div className={classNames(styles.input_bx,
                {[styles.input_bx_error]: errors.last_name})}> */}


                                            <Participant 
                                            participantData={el}
                                            gameCounter = 'no'/>
                                        </span>

                                    )
                                })}
                            </div>
                        </div>
                    </div> 
                </div>

                <div className={styles.btn__wrapper}>
                    <Btn 
                        onClick={handleClose}
                        type='button'
                    >
                      Назад
                    </Btn>
                    <Btn
                        onClick={() => {
                                if (selectedUser !== null || filterData.length === 1) {
                                    let selected = selectedUser ? selectedUser : 0
                                    handleSelect(filterData[selected])

                                } else {
                                    setError(true)
                                }
                            }}
                        btnType='outlined' 
                        type='button'
                    >Добавить
                    </Btn>
                </div>

            </ModalWrapper>
        </>
    )
}

export default AddParticipant
