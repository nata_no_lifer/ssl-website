// import {
//     createSlice,
//     createAsyncThunk,
//     SerializedError,
//   } from '@reduxjs/toolkit'
//   import { _apiUrl } from 'shared'
  
//   import type { TStatusType } from 'shared'
  
//   export interface InitDataAddParticipants {
//     token: string
//     id: number
//     typeRequest: 'add' | 'remove'
//   }
  
//   export const selectParticipants = createAsyncThunk(
//     'favouritesSlice/addToFavourites',
//     async ({ token, id, typeRequest }: InitDataAddParticipants) => {
//       let url: string = `/classes/game/`;
//       try {
//         const response = await fetch(`${_apiUrl}${url}`, {
//           method: 'POST',
//           headers: {
//             'Content-Type': 'application/json;charset=utf-8',
//             Authorization: `Token ${token}`,
//           },
//         })
//         if (response.ok) {
//           const data = await response.json()
//           return { data }
//         } else {
//           throw new Error(
//             `Could not fetch ${_apiUrl}/api/v1/user/, status: ${response.status}`
//           )
//         }
//       } catch (error) {
//         return
//       }
//     }
//   )
  
//   export interface IFavouritiesState {
//     neighbourLikeChangeStatus: TStatusType
//     roomLikeChangeStatus: TStatusType
//     errorFavourities: string | null | SerializedError
//   }
  
//   const initialState: IFavouritiesState = {
//     neighbourLikeChangeStatus: 'idle',
//     roomLikeChangeStatus: 'idle',
//     errorFavourities: null,
//   }
  
//   const favouritesSlice = createSlice({
//     name: 'favouritesSlice',
//     initialState,
//     reducers: {},
//     extraReducers: (builder) => {
//       builder
//         // fetchMyData
//         .addCase(selectParticipants.pending, (state) => {
//           state.neighbourLikeChangeStatus = 'loading'
//           state.roomLikeChangeStatus = 'loading'
//         })
//         // .addCase(toogleFavourites.rejected, (state, action) => {
//         //   if ('error' in action.payload) {
//         //     state.neighbourLikeChangeStatus = 'error'
//         //     state.roomLikeChangeStatus = 'error'
//         //     state.errorFavourities = action.payload.error
//         //   }
//         // })
//         .addCase(selectParticipants.fulfilled, (state, action) => {
//           if (action.meta.requestStatus === 'fulfilled') {
//             const { entity } = action.meta.arg
//             if (entity === 'users') {
//               state.neighbourLikeChangeStatus = 'success'
//             } else if (entity === 'rooms') {
//               state.roomLikeChangeStatus = 'success'
//             }
//           }
//         })
//     },
//   })
  
//   export default favouritesSlice.reducer
  