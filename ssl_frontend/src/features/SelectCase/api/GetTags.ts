import basePureRequest from "shared/api/basePureRequest";

type Tag = {
    id: number,
    type: string
}

export default async function GetTags(): Promise<string[]> {
    const response = await basePureRequest('classes/game_labels/', {});

    let result = await response.json();
    
    if (response.ok) { 
        return result.map((tagInfo: Tag): string => tagInfo.type);
    } else {
        console.log("error");
        // TODO handle errors
    }

    return [];
}