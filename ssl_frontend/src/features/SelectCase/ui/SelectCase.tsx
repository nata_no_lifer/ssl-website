import {useState, useEffect, useMemo} from 'react';
import { ModalWrapper, Title, Text, Btn, Case, CaseText, Overlay } from 'shared';

import find_icon from '../../../assets/icons/functional_icons/find.png';
import { CaseProps } from 'shared/types';

import classNames from 'classnames';
import styles from './SelectCase.module.scss';
import GetTags from '../api/GetTags';

interface Props {
    casesList: Array<CaseProps>
    isSelectCase: boolean
    handleClose: () => void
    className?: string
    handleSelect: (arg: CaseProps) => void
}

type TagType = {
    [tagName: string]: boolean
}

const SelectCase = (props: Props) => {
    const [selectedCase, setSelectedCase] = useState<number | null>(null);

    const [showCaseText, setShowCaseText] = useState<boolean>(false);

    const [caseTagList, setCaseTagList] = useState<TagType>({});

    const [searchWord, setSearchWord] = useState<string>('');

    const [error, setError] = useState<boolean>(false);

    const { casesList, isSelectCase, handleClose, handleSelect } = props;

    const filterData : CaseProps[] = useMemo(() => {
        setSelectedCase(null)
        setError(false);
        return casesList.filter((value: CaseProps): boolean => {
            const hasSearch : boolean = value.text.toLowerCase().includes(searchWord.toLowerCase()) 
            || value.title.toLowerCase().includes(searchWord.toLowerCase())
            || value.text.toLowerCase().includes(searchWord.toLowerCase())
            if (!hasSearch) return false

            for (let tag in caseTagList) {
                if (caseTagList[tag]) {
                   if (!value.tags.includes(tag)) return false
                }
            }

            return true
        })


    }, [casesList, searchWord, caseTagList])

    const [tags, setTags] = useState<Array<string>>([]);
    useEffect(() => { 
        GetTags().then((tags: Array<string>) => setTags(tags))
    }, []);

    console.log(caseTagList);

    return (
        <> 
        <Overlay isActive= {true} onClick={handleClose} className={styles.selectcase_overlay}/>
        {!showCaseText &&         
            <>
                    <div className={styles.selectcase__wrapper}>
                        <ModalWrapper 
                                    isModalOpen = {isSelectCase}
                                    closeModal={handleClose}
                                    showCloseBtn={false}
                                    wrapperClass={styles.wrapperModal}>
                            <div className={styles.input_bx}>
                                <input type="text" 
                                       placeholder='Поиск по кейсам' 
                                       className={styles.input} 
                                       onChange={(e) => setSearchWord(e.target.value)}>
                                </input>
                                <img src={find_icon} alt="find" className={styles.img} />
                            </div>
        
                            <div className={styles.selectcasetype_btn_group}>
                                {tags.map((tag: string) => 

                                    <button 
                                        className = {classNames(styles.selectcasetype_btn, {
                                            [styles.selectcasetype_btn_active]: caseTagList[tag]
                                        })}
                                        onClick = {() => setCaseTagList((old: TagType) => {
                                            return {
                                                ...old,
                                                [tag]: !old[tag]
                                            }
                                        })}
                                    >
                                        {tag}
                                    </button>)}
                            </div>
        
                            <div className={styles.caseslist_case__wrapper}>
                                                {filterData.map((el: CaseProps, id: number) => {
                                                    return (
                                                        <span onClick = {() => {
                                                                setSelectedCase(id);
                                                                setError(false);
                                                            }}>

                                                            
                                                              <div className={classNames(styles.caseslist__wrapper,
                                                             {[styles.caselist__wrapper_selected]: id === selectedCase})}>
                                                                <Case btn_close='hide' className={classNames(styles.case_num,
                                                             {[styles.case_num_selected]: id === selectedCase})}>{el.number.toString()}</Case>
                                                                <CaseText caseData = {el}></CaseText>
                                                                <button className={styles.case_btn} 
                                                                        onClick= {() => setShowCaseText(true)}>
                                                                        Показать весь текст
                                                                </button>
                                                            </div>
                                                        </span>
        
                                                    )
                                                })}
                            </div>
                        </ModalWrapper>
        
                      <div className={styles.selectcase_btn__wrapper}>
                                <Btn 
                                    onClick={handleClose}
                                    type='button'
                                    className={styles.selectcase_btn}
                                >
                                  Назад
                                </Btn>
                                <Btn
                                    onClick={() => {
                                            if (selectedCase !== null || filterData.length === 1) {
                                                let selected = selectedCase ? selectedCase : 0
                                                handleSelect(filterData[selected])
                                            } else {
                                                setError(true)
                                            }
                                        }}
                                    btnType='outlined' 
                                    type='button'
                                    className={styles.selectcase_btn}
                                >Добавить
                                </Btn>
                            </div>
                    </div>
                </>} 




            {showCaseText && selectedCase !== null &&  
                            <ModalWrapper   isModalOpen = {true}
                                            closeModal={handleClose}
                                            showCloseBtn={true}
                                            wrapperClass={styles.show_case__wrapper}>
                                <Title type='h3' className={styles.show_case_title}>{filterData[selectedCase].title}</Title>
                                <Text type = 'paragraph' className={styles.show_case_text}>{filterData[selectedCase].text}</Text>

                                <div className={styles.selectcase_btn__wrapper}>
                                    <Btn 
                                        onClick={() => setShowCaseText(false)}
                                        type='button'
                                        className={styles.selectcase_btn}
                                    >
                                      Назад
                                    </Btn>
                                    <Btn
                                        onClick={() => {
                                                if (filterData.length === 1) {
                                                    handleSelect(filterData[0])
                                                }

                                                if (selectedCase !== null) {
                                                    handleSelect(filterData[selectedCase])
                                                } else {
                                                    setError(true)
                                                }
                                            }}
                                        btnType='outlined' 
                                        type='button'
                                        className={styles.selectcase_btn}
                                    >Добавить
                                    </Btn>
                                </div>
                            </ModalWrapper> 
}

        </>

    )
}

export default SelectCase
