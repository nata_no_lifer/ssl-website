import { FormEvent, useState } from 'react';
import { Navigate } from "react-router-dom";
import Cookies from 'js-cookie';

import { Btn, routes, url } from 'shared';
import closeImg from 'assets/icons/functional_icons/close1.png';
import { GeneralInformation, ProfileInformation, ConfirmCode, Success } from 'entites';

import styles from './NewRegistrationForm.module.scss';


function RegistrationForm() {
    const [step, setStep] = useState(0);
    const [redirect, setRedirect] = useState(false);

    const [image, setImage] = useState('');

    const [surname, setSurname] = useState('');
    const [surnameError, setSurnameError] = useState('');

    const [name, setName] = useState('');
    const [nameError, setNameError] = useState('');

    const [fathername, setFathername] = useState('');
    const [fathernameError, setFathernameError] = useState('');

    const [hsePass, setHsePass] = useState(false);

    const [conditions, setConditions] = useState(false);
    const [conditionsError, setConditionsError] = useState(false);

    const [email, setEmail] = useState('');
    const [emailError, setEmailError] = useState('');

    const [telegram, setTelegram] = useState('');
    const [telegramError, setTelegramError] = useState('');

    const [password, setPassword] = useState('');
    const [passwordError, setPasswordError] = useState('');

    const [repeatedPassword, setRepeatedPassword] = useState('');
    const [repeatedPasswordError, setRepeatedPasswordError] = useState('');

    const [code, setCode] = useState('');
    const [codeError, setCodeError] = useState('');

    const [isAuth, setIsAuth] = useState(Cookies.get('token') ? true : false);


    const clickHandler = () => {
        if ((step === 3)) {
            setRedirect(true);
        }
    }

    const RedirectHandler = () => {
        if (isAuth) {
            return <Navigate to={routes.userProfilePage} replace={true} />
        } else if ((step === 3) && (redirect)) {
            return <Navigate to={routes.authPage} replace={true} />
        } else {
            return <></>
        }
    }


    const registerUser = async () => {
        const profile = new FormData();
        profile.append('first_name', name); 
        profile.append('last_name', surname);
        profile.append('father_name', fathername); 
        profile.append('telegram', telegram);
        profile.append('email', email);
        profile.append('password1', password);
        profile.append('password2', repeatedPassword);
        profile.append('hse_pass', 'false');
        profile.append('accept_conditions', 'true');
        if (image) profile.append('image', image);


        const response = await fetch(`${url}/users/auth/login/` ,{
            method: 'POST',
            body: profile
        });

        const json = await response.json();

        return {
            ok: response.ok,
            errors: response.ok ? {} : json.errors
        }
    }

    const verifyEmail = async () => {
        const way = `${url}/users/verify_email/` + email + '/';
        const response = await fetch(way ,{
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({code: code})
        });

        const json = await response.json();

        return {
            ok: response.ok,
            error: response.ok ? {} : json['Wrong code']
        }
    }


    function mainErrorHandler(errors: any) {
        if (
            errors.last_name || 
            errors.first_name || 
            errors.father_name || 
            errors.accept_conditions
        ) setStep((currentStep) => 0);

        setSurnameError(errors.last_name);
        setNameError(errors.first_name);
        setFathernameError(errors.father_name);
        setConditionsError(errors.accept_conditions);
        setEmailError(errors.email);
        setTelegramError(errors.telegram);
        setPasswordError(errors.password1);
        setRepeatedPasswordError(errors.password);
    }


    function emailErrorHandler(error: any) {
        setCodeError(error);
    }


    async function submit(event: FormEvent) {
        event.preventDefault();

        switch(step) {
            case 0:
                setStep((currentStep) => currentStep + 1);
                break;

            case 1:
                const resultRegister = await registerUser();
                if (resultRegister.ok) {
                    setStep((currentStep) => currentStep + 1);
                } else {
                    console.log(resultRegister.errors);
                    mainErrorHandler(resultRegister.errors);
                }
                break;
                
            case 2:
                const resultVerifyEmail = await verifyEmail();
                if (resultVerifyEmail.ok) {
                    setStep((currentStep) => currentStep + 1);
                } else {
                    emailErrorHandler(resultVerifyEmail.error);
                }
                break;
        }
    }

    function UpButtons() {
        if (step === 1 || step === 2) {
            return (
                <div className={styles.upButtons}>
                    <div className={styles.back} onClick={() => setStep((currentStep) => currentStep - 1)}>Назад</div>
                    <div className={styles.close}>
                        <img src={closeImg} alt="Close" />
                    </div>
                </div>
            )
        }
    }


    function buttonText() {
        if (step < 3) return "Продолжить"
        else if (step === 3) return "Закрыть"
    }

    function resend() {

    }


    function Switch(step: number) {
        switch(step) {
            case 0:
                return(
                    <GeneralInformation 
                        image={{image, setImage}}
                        surname={{surname, setSurname, surnameError, setSurnameError}}
                        name={{name, setName, nameError, setNameError}}
                        fathername={{fathername, setFathername, fathernameError, setFathernameError}}
                        hsePass={{hsePass, setHsePass}}
                        conditions={{conditions, setConditions, conditionsError, setConditionsError}}
                    />
                );
            case 1:
                return(
                    <ProfileInformation
                        email={{email, setEmail, emailError, setEmailError}}
                        telegram={{telegram, setTelegram, telegramError, setTelegramError}}
                        password={{password, setPassword, passwordError, setPasswordError}}
                        repeatedPassword={{repeatedPassword, setRepeatedPassword, repeatedPasswordError, setRepeatedPasswordError}}
                    />
                );
            case 2:
                return(
                    <ConfirmCode
                        code={{code, setCode, codeError, setCodeError}}
                        resend={resend}
                    />
                );
            case 3:
                return(
                    <Success
                        email={email} 
                    />
                );
        }
    }


    return (
        <>
            <RedirectHandler />
            <form onSubmit={submit}>
                { UpButtons() }
                { Switch(step) }
                <Btn type='submit' onClick={clickHandler}>{buttonText()}</Btn>
            </form>
        </>
    )
}


export default RegistrationForm;