import {participantsFetched, participantsFetching, participantsFetchingError, participantsCreated, participantsChangeGames, participantsSetState} from "../../entites/Participant/model/participantsSlice"
import {casesFetching, casesFetched, casesFetchingError} from "../../entites/Case/model/caseSlice"
import {tablesFetching, tablesFetched, tablesFetchingError,
        tablesAddReferee, tablesDeleteReferee, tablesAddGame, tablesCreated, tablesDeleted, tablesChangeName, tablesUpdateGame} from "../../entites/TableList/model/tableSlice"
import { usersDeleted, usersFetched, usersFetchingError } from "entites/User/model/usersSlice";

export const fetchParticipants = (request) => (dispatch) => {
    dispatch(participantsFetching());
    request("classes/game_register/")
        .then(data => dispatch(participantsFetched(data)))
        .catch(() => dispatch(participantsFetchingError()))
}

export const addUnregisterUser = (request, unregister) => (dispatch) => {
    request("classes/game_register/", { method: "POST", body: JSON.stringify({player: unregister.id}) })
        .then(data => {
            request(`classes/game_register/${data.id}/`)
            .then(resp => dispatch(participantsCreated(resp)))
            .catch(() => console.log("add unregister user error"))
            
            dispatch(usersDeleted(unregister.id))
        })
        .catch(() => console.log("add unregister user error"))
}

export const fetchUsers = (request) => (dispatch) => {
    dispatch(participantsFetching());
    request("classes/get_unregisted/")
        .then(data => dispatch(usersFetched(data)))
        .catch(() => dispatch(usersFetchingError()))
}


export const fetchCases = (request) => (dispatch) => {
    dispatch(casesFetching());
    request("classes/cases/")
        .then(data => dispatch(casesFetched(data)))
        .catch(() => dispatch(casesFetchingError()))
}

export const fetchTables = (request) => (dispatch) => {
    dispatch(tablesFetching());
    request("classes/get_info_table/")
        .then(data =>{
            if (data.length === 0) {
                request(`classes/table/`, { method: "POST"})
                .then(resp => null)
                .catch(() => console.log("classes/table error"))

                dispatch(fetchTables(request))
                return 
            }

            dispatch(tablesFetched(data))
        } )
        .catch(() => dispatch(tablesFetchingError()))
}

export const addReferee = (request, table, register) => (dispatch) => {
    request("classes/referee/", { method: "POST", body: JSON.stringify({table: table.id, referee: register.id}) })
        .then(data => dispatch(tablesAddReferee({id: table.id, register: data.referee.game_registration.id, referee: data.id})))
        .catch(() => dispatch(tablesFetchingError()))
}

export const deleteReferee = (request, table, referee) => (dispatch) => {
    request(`classes/referee/${referee.refereeId}/`, {method: "DELETE"}, true)
        .then(data => dispatch(tablesDeleteReferee({id: table.id, referee: referee.refereeId })))
        .catch(() => dispatch(tablesFetchingError()))
}

export const addGame = (request, tableID, gameType) => (dispatch) => {
    /// TODO: lib
    let mapper = {
        'conflicts': 'Конфликт',
        'negotiations': 'Переговоры',       
    }
    request("classes/game/", { method: "POST", body: JSON.stringify({table: tableID, game_type: mapper[gameType]}) })
        .then(data => dispatch(tablesAddGame({id: tableID, type: gameType, gameID: data.id})))
        .catch(() => dispatch(tablesFetchingError()))
}

export const addTable = (request, name) => (dispatch) => {
    request("classes/table/", { method: "POST", body: JSON.stringify({number: name}) })
        .then(data => dispatch(tablesCreated({id: data.id, name: data.number})))
        .catch(() => dispatch(tablesFetchingError()))
}

export const deleteTable = (request, table) => (dispatch) => {
    request(`classes/table/${table.id}/`, { method: "DELETE" }, true)
        .then(resp => dispatch(tablesDeleted({id: table.id})))
        .catch(() => dispatch(tablesFetchingError()))
}

export const changeTableName = (request, table, newName) => (dispatch) => {
    request(`classes/table/${table.id}/`, { method: "PATCH", body: JSON.stringify({number: newName}) })
        .then(data => dispatch(tablesChangeName({id: data.id, name: data.number})))
        .catch(() => dispatch(tablesFetchingError()))
}


export const setStatus = (request, register, status) => (dispatch) => {
    
    if ((status === 'Не пришел' && register.attendance === 'Не пришел') || (status === 'Опоздал' && register.attendance === 'Опоздал')) {
        request(`classes/game_register/${register.id}/`, { method: "PATCH", body: JSON.stringify({number: register.id, attendance: 'Сыграл'}) })
        .then(data => dispatch(participantsSetState({id: register.id, status: data.attendance})))
        .catch(() => dispatch(participantsFetchingError()))
    }
    else {
    request(`classes/game_register/${register.id}/`, { method: "PATCH", body: JSON.stringify({number: register.id, attendance: status}) })
        .then(data => dispatch(participantsSetState({id: register.id, status: data.attendance})))
        .catch(() => dispatch(participantsFetchingError()))

    }
}

export const changeGame = (request, newGame, gameType, type) => (dispatch) => {
    let mapper = {
        'conflicts': 'Конфликт',
        'negotiations': 'Переговоры',       
    }
    let fetchData = {
        id: newGame.id,
        table: newGame.table,
        game_type: mapper[newGame.gameType],
    }

    if ('case' in newGame) {
        fetchData.case = type === 'add' ? newGame.case : null
    }

    if ('participant1' in newGame) {
        fetchData.player1 = type === 'add' ? newGame.participant1 : null
    }

    if ('participant2' in newGame) {
        fetchData.player2 = type === 'add' ? newGame.participant2 : null
    }

    if ('player1Score' in newGame) {
        fetchData.player1_score = newGame.player1Score
    }

    if ('player2Score' in newGame) {
        fetchData.player2_score = newGame.player2Score
    }

    request(`classes/game/${newGame.id}/`, { method: "PATCH", body: JSON.stringify(fetchData) })
        .then(data => {
            let tableChange = {
                id: newGame.id,
            }
            if ('participant1' in newGame) {
                tableChange.participant1 = type === 'add' ? newGame.participant1 : null
                dispatch(participantsChangeGames({id: newGame.participant1, type: gameType, delta: type === 'add' ? 1: - 1}))
            }

            if ('participant2' in newGame) {
                tableChange.participant2 = type === 'add' ? newGame.participant2 : null
                dispatch(participantsChangeGames({id: newGame.participant2, type: gameType, delta: type === 'add' ? 1: - 1}))
            }

            if ('case' in newGame) {
                tableChange.case = type === 'add' ? newGame.case : null
            }

            if ('player1Score' in newGame) {
                tableChange.player1Score = newGame.player1Score
            }

            if ('player2Score' in newGame) {
                tableChange.player2Score = newGame.player2Score
            }
 
            dispatch(tablesUpdateGame({id: newGame.table, type: gameType, game: tableChange, changePayload: {type: type, change: newGame}}))
         })
        .catch(() => dispatch(tablesFetchingError()))
}
