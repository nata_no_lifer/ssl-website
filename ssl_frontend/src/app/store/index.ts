import { configureStore } from '@reduxjs/toolkit';
import participants from 'entites/Participant/model/participantsSlice';
import cases from 'entites/Case/model/caseSlice'
import tables from 'entites/TableList/model/tableSlice'
import users from 'entites/User/model/usersSlice'

const stringMiddleware = () => (next: any) => (action: any) => {
    if (typeof action === 'string') {
        return next({
            type: action
        })
    }
    return next(action)
};

const store = configureStore({
    reducer: {participants, cases, tables, users},
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(stringMiddleware),
    devTools: process.env.NODE_ENV !== 'production',
})

export default store;

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch
