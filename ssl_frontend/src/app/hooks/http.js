import { useState, useCallback } from "react";
import basePureRequest from "shared/api/basePureRequest";

export default function useHttp() {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const request = useCallback(async (url, options, emptyResponse=false) => {

        setLoading(true);

        try {
            const response = await basePureRequest(url, options);

            const data = emptyResponse ? null : await response.json();

            if (!response.ok) {
                throw new Error(data.error);
            }

            setLoading(false);
            return data;
        } catch(e) {
            setLoading(false);
            setError(e.message);
            throw e;
        }
    }, []);

    const clearError = useCallback(() => setError(null), []);

    return {loading, request, error, clearError}
}