import {useState} from 'react';
import { GameInfo } from 'entites';
import {SelectCase} from 'features/SelectCase';
import styles from './AllTablesItemNet.module.scss';
import { IGameInfo, ParticipantProps, CaseProps } from 'shared/types';

interface NetProps {
  tableGames: Array<IGameInfo> | undefined
  usersList: Array<ParticipantProps>
  setTableGames: React.Dispatch<React.SetStateAction<IGameInfo[]>>
  gameType: 'conflicts' | 'negotiations'
  casesList: Array<CaseProps>
}

function AllTablesItemNet(netProps: NetProps) {
    const { tableGames, setTableGames, casesList, gameType } = netProps;
    const [modalElement, setModalElement] = useState<{element: 'participant1' | 'participant2', id: number}>({
        element: 'participant1',
        id: 0,
    });

    const [isCaseModalOpen, setIsCaseModalOpen] = useState<boolean>(false)

    return (
        <div className={styles.net__wrapper}>
            {tableGames?.map((el: IGameInfo, id: number) => 
                    <GameInfo 
                        gameType={gameType} gameInfo={el} pageType='admin'
                        openModal={(el: 'participant1' | 'participant2') => setModalElement({ element: el, id: id }) }
                        openCaseModal={() => { setIsCaseModalOpen(true); setModalElement({ element: modalElement.element, id: id }); } }

                        handleDelete={() => {
                            setTableGames((old: Array<IGameInfo>) => old.map((el: IGameInfo, id_case: number) => {
                                if (id === id_case) {
                                    return {
                                        ...el,
                                        ...{ case: null }
                                    };
                                }
                                return el;
                            }));
                        }}
                        handleSetScore={() => {}}
                    />
            )}
            {isCaseModalOpen && <SelectCase 
                casesList={casesList}
                isSelectCase = {isCaseModalOpen}
                handleClose={() => setIsCaseModalOpen(false)}
                handleSelect={(arg: CaseProps) => {
                    setTableGames((old: Array<IGameInfo>) => old.map((el:IGameInfo, id: number)=> {
                        if(id === modalElement.id) {
                            return {
                              ...el,
                              ...{case: arg}
                            }
                        } 
                        return el;
                    }))
                    setIsCaseModalOpen(false)
                    }
                }
                className = {styles.select_user} 
                />
            }
        </div>
    )
}

export default AllTablesItemNet
