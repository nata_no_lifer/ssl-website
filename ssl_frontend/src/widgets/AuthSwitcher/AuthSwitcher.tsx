import { NavLink } from "react-router-dom";
import classNames from "classnames";
import { routes } from 'shared';

import styles from './Authswitcher.module.scss';

function AuthSwitcher(props: any) {
    const {login, registration} = props;

    return (
        <div className={styles.container}>
            <div className={classNames([styles.tab, login ? styles.active : null])}>
                <NavLink to={routes.authPage}>Вход</NavLink>
            </div>

            <div className={classNames([styles.tab, registration ? styles.active : null])}>
                <NavLink to={routes.regPage}>Регистрация</NavLink>
            </div>
        </div>
    )
}

export default AuthSwitcher;