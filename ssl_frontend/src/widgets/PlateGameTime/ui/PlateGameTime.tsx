import React from 'react';
import { SetGameRegistrationTime } from 'features/SetGameRegistrationTime';

import styles from './PlateGameTime.module.scss';

const PlateGameTime = () => {
  
  return (
    <div className={styles.form__wrapper}>
        <SetGameRegistrationTime/>
    </div>
  )
}

export default PlateGameTime;

