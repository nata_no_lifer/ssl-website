import { ReactElement } from "react";
import styles from './FormGreyBackground.module.scss';

function FormGreyBackground(props: any) {
    return (
        <div className={styles.background}>
            {props.children}
        </div>
    )
}

export default FormGreyBackground;