import { NavLink } from "react-router-dom";
import { routes } from "shared";

import headerLogo from '../../assets/icons/logo_icons/header_logo.png'
import styles from './Header.module.scss';


function Header(props: any) {

    const setLinks = (x: number) => {
        switch(x) {
            case 1:
                return (
                    <>
                        <NavLink to={routes.mainPage}>Главная</NavLink>
                        <NavLink to={routes.nextGamePage}>Предстоящая игра</NavLink>
                        <NavLink to={routes.pastGamePage}>Прошедшие игры</NavLink>
                        <NavLink to={routes.rulesPage}>Правила</NavLink>
                        <NavLink to={routes.ratingPage}>Рейтинг</NavLink>
                    </>
                );
            case (2 || 3):
                return (
                    <>
                        <NavLink to="">Сетка</NavLink>
                        <NavLink to="">База данных</NavLink>
                        <NavLink to="">Кейсы</NavLink>
                    </>
                );
        }
    }


    return (
        <header className={styles.header}>
            <div className={styles.wrapper}>
                <div className={styles.buttons}>
                    <img src={headerLogo} alt="" className={styles.logo} />
                    <nav className={styles.nav}>
                        {setLinks(+props.userRole)}
                    </nav>
                </div>
                
                <NavLink to={routes.userProfilePage}>
                    <div className={styles.profile}>
                        <img src={props.image} alt="User" className={styles.image} />
                    </div>
                </NavLink>
            </div>
        </header>
    )
}


export default Header;