
import PlateGameTime from "./PlateGameTime/ui/PlateGameTime";
import Footer from "./Footer/ui/Footer";
import Net from "./Net/ui/Net";
// import RefereeNet from "./RefereeNet/RefereeNet";
import Table from './Table/ui/Table';
import AllTablesItem from "./AllTablesItem/ui/AllTablesItem";
import RegisteredUsers from "./RegisteredUsers/RegisteredUsers";
import Header from "./Header/Header";
import NewFooter from "./NewFooter/NewFooter";
import AuthSwitcher from "./AuthSwitcher/AuthSwitcher";
import FormGreyBackground from './FormGreyBackground/FormGreyBackground';

export {
    PlateGameTime,
    Footer,
    Net,
    // RefereeNet,
    Table,
    AllTablesItem,
    RegisteredUsers,
    Header,
    NewFooter,
    AuthSwitcher,
    FormGreyBackground
}