import {useState} from 'react';
import {Title} from 'shared';
import { GameInfo } from 'entites';
import styles from './AllTablesItem.module.scss';
import { IGameInfo, TableProps } from 'shared/types';
import { CaseType } from 'shared/types/case';
import Tabs, { ITab } from 'shared/ui/Tabs/Tabs';
import { RefereeList } from 'entites/net';



interface NetProps {
    table: TableProps
    pageType: "referee" | "admin"
}

function AllTablesItem(netProps: NetProps) {
    const {pageType, table } = netProps;


    // referee head part
    const [gameType, setGameType] = useState<CaseType>('conflicts');
    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

    const tabs: ITab[] = [
        {id: 0, label: 'Конфликты'},
        {id: 1, label: 'Переговоры'}
    ];

    const gameTypeToId: {[gameType: string]: number} = {
        'conflicts': 0,
        'negotiations': 1,     
    }

    const idToGameType: {[id: number]: CaseType} = {
        0: 'conflicts',
        1: 'negotiations',     
    }

    return (
        <div className={styles.net__wrapper}>    
                <p className={styles.table__input_bx}>{table.tableName}</p>

            <Tabs selectedId={gameTypeToId[gameType]} 
                  tabs={tabs}
                  onClick={(id: number) => setGameType(idToGameType[id])} 
                  className={styles.tabs} 
                  tabClassName={styles.tab}
            />
            <div className={styles.devider}></div>

            <Title type="h3" font_style='bold'>Арбитры</Title>

            <RefereeList 
                    isEdited = {false}
                    refereeList={table.referees}
                    openModal={() => setIsModalOpen(true)}
                    statusType='show'
                    table={table}
                />

            {table[gameType].map((game: IGameInfo, id: number) =>
                <span key={id}>
                    <GameInfo
                        gameType = {gameType} gameInfo={game} pageType={pageType}
                        openModal = {() => {}}
                        openCaseModal={() => {}}

                        handleDelete={() => {}}
                        handleSetScore={() => {}}
                    />
                </span>
            )}

        </div>
    )
}

export default AllTablesItem