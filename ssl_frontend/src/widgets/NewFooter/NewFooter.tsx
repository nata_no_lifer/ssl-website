import headerLogo from '../../assets/icons/logo_icons/header_logo.png'
import styles from './NewFooter.module.scss';

function NewFooter(props: any) {
    return (
        <div className={styles.footer}>
                    <div className={styles.firstLine}>
                        <div className={styles.footer_email}>ssl@gmai.com</div>
                        <div className={styles.footer_squares}>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </div>

                    <div className={styles.secondLine}>Пользовательское соглашение</div>

                    <div className={styles.thirdLine}>
                        <div className={styles.footer_agreement}>Политика обработки персональных данных</div>
                        <div className={styles.footer_logo}>© Soft Skills Lab</div>
                    </div>
            </div>
    )
}

export default NewFooter;