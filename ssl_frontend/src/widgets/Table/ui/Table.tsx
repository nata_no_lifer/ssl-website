import { useEffect, useMemo, useState } from 'react';
import { RefereeList, TableList } from 'entites/net';
import { Title, Btn, Tabs } from 'shared';
import { SelectUser } from 'features/SelectUser';
import { ITab } from '../../../shared/ui/Tabs/Tabs'
import { ParticipantProps, TableProps } from 'shared/types';

import styles from './Table.module.scss';
import { useDispatch } from 'react-redux';
import useHttp from 'app/hooks/http';
import { addReferee, changeTableName } from 'app/actions';
import { CaseType } from 'shared/types/case';
import refereeEmphasisCondition from '../lib/refereeEmphasisCondition';
import sortReferee from '../lib/sortReferee';


interface Props {
    participantsList: Array<ParticipantProps>
    tableList: Array<TableProps>
    setSelectedTableId: React.Dispatch<React.SetStateAction<number>>
    selectedTableId: number
    gameType: 'conflicts' | 'negotiations'
    setGameType: React.Dispatch<React.SetStateAction<CaseType>>
}

function Table(props: Props) {
    const { participantsList, tableList, selectedTableId, gameType, setGameType} = props

    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

    const tabs: ITab[] = [
        {id: 0, label: 'Конфликты'},
        {id: 1, label: 'Переговоры'}
    ];

    const gameTypeToId: {[gameType: string]: number} = {
        'conflicts': 0,
        'negotiations': 1,     
    }

    const idToGameType: {[id: number]: CaseType} = {
        0: 'conflicts',
        1: 'negotiations',     
    }

    const dispatch = useDispatch();
    const { request } = useHttp();

    const [tableName, setTableName] = useState<string>();
    useEffect(() => {
        setTableName(tableList[selectedTableId].tableName)
    }, [tableList, selectedTableId])

    const possibleReferees = useMemo<ParticipantProps[]>(() => {
        return participantsList.filter((participant: ParticipantProps): boolean => {
            return participant.role === 'arbitrator'
        })
    }, [participantsList, tableList, selectedTableId])

    return (
        <div className={styles.table__wrapper}>
            <div>
                <input
                    className={styles.table__input_bx}
                    type="text"
                    autoComplete="off"
                    defaultValue={tableName}
                    placeholder= {`Стол ${selectedTableId + 1}`}
                    onChange={e => setTableName(e.target.value)}
                    //@ts-ignore
                    onBlur={e => dispatch(changeTableName(request, tableList[selectedTableId], e.target.value))}
                />
            </div>

            <Tabs selectedId={gameTypeToId[gameType]} 
                  tabs={tabs}
                  onClick={(id: number) => setGameType(idToGameType[id])} 
                  className={styles.tabs} 
                  tabClassName={styles.tab}
            />

            <div className={styles.Table_block}>
                <TableList {...props}/>
                <a href="/classes/tables">
                    <Btn btnType='outlined' 
                         color = 'blue-main' 
                         width={180}
                         height={40}
                         className={styles.fs_14}
                    >
                        Посмотреть все столы
                    </Btn>
                </a>
            </div>

            <div className={styles.devider}></div>
            
            <div className={styles.referee_block}>
                <Title type="h3" 
                       font_style='bold' 
                       margin='m0_auto' 
                       className={styles.ta_s}
                >
                    Арбитры
                </Title>

                <RefereeList 
                    refereeList={tableList[selectedTableId].referees}
                    openModal={() => setIsModalOpen(true)}
                    statusType='show'
                    table={tableList[selectedTableId]}
                />
              
                {isModalOpen && <SelectUser
                                    comparator={sortReferee(tableList[selectedTableId].referees)}
                                    emphasisCondition={refereeEmphasisCondition(tableList[selectedTableId].referees)}
                                    usersList={possibleReferees}
                                    isSelectUser={isModalOpen}
                                    handleClose={() => setIsModalOpen(false)}
                                    handleSelect={async (referee: ParticipantProps) => {
                                        try {
                                            //@ts-ignore
                                            dispatch(addReferee(request, tableList[selectedTableId], referee))
                                            setIsModalOpen(false)
                                        } catch(err) {
                                            console.log(err)
                                        }
                                    }}
                                    className={styles.select_user}
                              />
                }
            </div>
        </div>
    )
}

export default Table
