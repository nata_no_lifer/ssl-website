import { BaseRequest } from "shared"

export interface Referee {
    table?: number
    referee?: number
}


export default async function SendReferee (props: Referee) {
    let method = "POST"
    let url = 'classes/referee/';
   
    let {result, ok} = await BaseRequest(url, {method: method, body:JSON.stringify(props)})

    if (ok) {
        console.log("Арбитор успешно добавлен", result)
        return result
    } else {
        console.log("рефери БЭК момэнт");
        throw result
        // TODO handle errors
    }
}
