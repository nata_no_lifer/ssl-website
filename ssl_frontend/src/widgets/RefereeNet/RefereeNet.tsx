// import React, {useState} from 'react';
// import {Title, NewElementBtn} from 'shared';
// import { GameInfo } from 'entites';
// import { SelectUser } from 'features/SelectUser';
// import {SelectCase} from 'features/SelectCase';
// import styles from './RefereeNet.module.scss';
// import { IGameInfo, ParticipantProps, CaseProps } from 'shared/types';
// import SendGame from 'widgets/Net/api/SendGame';

// interface NetProps {
//   tableGames: Array<IGameInfo> | undefined
//   usersList: Array<ParticipantProps>
//   setTableGames: any

//   casesList: Array<CaseProps>
//   // selectedCaseId: number
// }

// function RefereeNet(netProps: NetProps) {
//   const { usersList, tableGames, setTableGames, casesList } = netProps;

//   const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
//   const [modalElement, setModalElement] = useState<{element: 'participant1' | 'participant2', id: number}>({
//     element: 'participant1',
//     id: 0
//   });

//   const [isCaseModalOpen, setIsCaseModalOpen] = useState<boolean>(false)
//   // const [data, setData] = useState(0);

//   // function updateFields(fields:Partial<FormData>) {
//   //     setData(prev => {
//   //         return { ...prev, ...fields}
//   //     })
//   // }

//   return (
//     <div className={styles.net__wrapper}>
//         <Title type="h3" font_style='bold'>Сетка игроков</Title>
//         {tableGames?.map((el: IGameInfo, id: number) => <GameInfo gameInfo={el}
//         //  updateFields = {updateFields}
//         openModal={(el: 'participant1' | 'participant2') => { setIsModalOpen(true); setModalElement({ element: el, id: id }); } }
//         openCaseModal={() => { setIsCaseModalOpen(true); setModalElement({ element: modalElement.element, id: id }); } } pageType={'referee'} handleDelete={undefined} gameType={'conflicts'}        />)}
//         {isModalOpen && <SelectUser
//                                       comparator='conflicts'
//                                       usersList={usersList}
//                                       isSelectUser = {isModalOpen}
//                                       handleClose={() => setIsModalOpen(false)}
//                                       handleSelect={(user: ParticipantProps) => {
//                                         setTableGames((old: Array<IGameInfo>) => old.map((el: IGameInfo, id: number) => {
//                                            if (id === modalElement.id) {
//                                                 return {
//                                                   ...el,
//                                                   ...{[modalElement.element]: user}
//                                                 }
//                                            }

//                                            return el;
//                                         }))
//                                         setIsModalOpen(false)
//                                       }} className = {styles.select_user}
                                      
//                                       />
//           }
//         {isCaseModalOpen && <SelectCase casesList={casesList}
//                                       isSelectCase = {isCaseModalOpen}
//                                       handleClose={() => setIsCaseModalOpen(false)}
//                                       handleSelect={(arg: CaseProps) => {
//                                         setTableGames((old: Array<IGameInfo>) => old.map((el:IGameInfo, id: number)=> {
//                                            if(id === modalElement.id) {
//                                                 return {
//                                                   ...el,
//                                                   ...{case: arg}
//                                                 }
//                                             } 
//                                             return el;
//                                         }))
//                                         setIsCaseModalOpen(false)
//                                       }} className = {styles.select_user} 
                                          
//                                       />
//           }

//           <NewElementBtn className={styles.add_empty_game} onClick={() => {
//           // @ts-ignore
//           let newGame: Game = SendGame({table: tableID, game_type: gameType === 'conflicts' ? 'Конфликты' : 'Переговоры'});

//           let game: IGameInfo = {
//             id: newGame.id,
//             participant1: null,
//             participant2: null,
//             case: null
//           }
//           setTableGames((old: Array<IGameInfo>) => [...old, {game}])}
//         }/>
//     </div>
//   )
// }

// export default RefereeNet
