import {Title, Text} from 'shared';
import styles from './Footer.module.scss';

function Footer() {
    return (
        <>
            <footer className= {styles.footer}>
                <div className={styles.contact_info}>
                    <Title type="h2" margin='m0_auto'>
                        ssl@gmai.com
                    </Title>
                    
                    <div className={styles.icons__wrapper}>
                        <div className={styles.icon_load}></div>
                        <div className={styles.icon_load}></div>
                        <div className={styles.icon_load}></div>
                        <div className={styles.icon_load}></div>

                    </div>
                </div>

                <div className={styles.main_info}>
                    <div className={styles.links__wrapper}>
                        <Text type='paragraph'>Пользовательское соглашение</Text>
                        <Text type = 'paragraph'>Политика обработки персональных данных</Text>
                    </div>
                    <Title type = 'h3'>© Soft Skills Lab</Title>
                </div>


            </footer>
        </>
    )
}

export default Footer
