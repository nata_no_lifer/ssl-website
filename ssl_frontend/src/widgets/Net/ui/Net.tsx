import {useMemo, useState} from 'react';
import {Title, NewElementBtn, Btn, askAgain} from 'shared';
import { GameInfo } from 'entites';
import { SelectUser } from 'features/SelectUser';
import {SelectCase} from 'features/SelectCase';
import styles from './Net.module.scss';
import { IGameInfo, ParticipantProps, CaseProps } from 'shared/types';
import { CaseType } from 'shared/types/case';
import { useDispatch } from 'react-redux';
import useHttp from 'app/hooks/http';
import { addGame, changeGame } from 'app/actions';
import SendNetModal from 'entites/net/components/Modals/SendNetModal';
import SendNetModalSuccess from 'entites/net/components/Modals/SendNetModalSuccess';
import SendNetModalError from 'entites/net/components/Modals/SendNetModalError';
import timer from '../../../assets/icons/functional_icons/timer.svg';
import sortUsersByGameType from 'features/SelectUser/lib/sortUsersByGameType';
import emphasisCondition from 'features/EmphasisCondition/emphasisCondition';


interface NetProps {
  tableID: number
  gameType: CaseType
  tableGames: Array<IGameInfo> | undefined
  usersList: Array<ParticipantProps>
  pageType: "referee" | "admin"
  casesList: Array<CaseProps>
}

function Net(netProps: NetProps) {
    const { tableID, gameType, usersList, tableGames, casesList, pageType } = netProps;

    const [isParticipantModalOpen, setIsParticipantModalOpen] = useState<boolean>(false);
    const [isSendNetModalOpen, setIsSendNetModalOpen] = useState<boolean>(false)
    const [isSendNetModalOpenResponse, setIsSendNetModalOpenResponse] = useState<'success' | 'failure' | 'hide'>('hide')

    const [modalElement, setModalElement] = useState<{element: 'participant1' | 'participant2' | 'case', id: number}>({
        element: 'participant1',
        id: 0
    });

    const [isCaseModalOpen, setIsCaseModalOpen] = useState<boolean>(false)

    const dispatch = useDispatch();
    const {request, error} = useHttp();

    const timerMap = new Map([
      ["conflicts", "https://negotiate.school/useful/timer/1"],
      ["negotiations", "https://negotiate.school/useful/timer/4"]
    ]);
    
    const TypeFilteredCaseList = useMemo(() => casesList.filter((value: CaseProps) => {
        if (gameType === 'conflicts') {
          return value.type.includes('conflicts');
        }
        return value.type.includes('negotiations');
        
   }), [casesList, gameType])

    return (
        <div className={styles.net__wrapper}>
            <Title type="h3" font_style='bold'>Сетка игроков</Title>

            {pageType === 'referee' && gameType === 'conflicts'  
                ? <a href={timerMap.get("conflicts")} className={styles.net__timer_conflicts}>
                    <img src={timer} alt="timer" />
                    Таймер
                  </a>
                : null
            }

            {pageType === 'referee' && gameType === 'negotiations'  
                ? <a href={timerMap.get("negotiations")} className={styles.net__timer_negotiations}>
                    <img src={timer} alt="timer" />
                    Таймер
                  </a>
                : null
            }


            {tableGames?.map((game: IGameInfo, id: number) =>
                <span key={id}>
                    <GameInfo
                        gameType = {gameType} gameInfo={game} pageType={pageType}
                        openModal = {(participant: 'participant1' | 'participant2') => {setIsParticipantModalOpen(true); setModalElement({element: participant, id: id})}}
                        openCaseModal={() => {setIsCaseModalOpen(true); setModalElement({element: 'case', id: id})}}

                        handleDelete={(element: 'case' | 'participant1' | 'participant2') => {
                            //@ts-ignore
                            dispatch(changeGame(request, {id: game.id, table: tableID, [element]: game[element].id, gameType: gameType}, gameType, 'sub'))
                        }}
                        handleSetScore={(element: 'player1Score' | 'player2Score', value: number) => {
                            //@ts-ignore
                            dispatch(changeGame(request, {id: game.id, table: tableID, [element]: value, gameType: gameType}, gameType, 'add'))
                        }}
                    />
                </span>
            )}

            {isParticipantModalOpen && <SelectUser
                                      comparator = {sortUsersByGameType(gameType)}
                                      emphasisCondition={emphasisCondition(gameType)}
                                      usersList={usersList}
                                      isSelectUser = {isParticipantModalOpen}
                                      handleClose={() => setIsParticipantModalOpen(false)}
                                      handleSelect={(user: ParticipantProps) => {
                                            const game = tableGames![modalElement.id]
                                            //@ts-ignore
                                            dispatch(changeGame(request, {id: game.id, table: tableID, [modalElement.element]: user.id, gameType: gameType}, gameType, 'add'))
                                            setIsParticipantModalOpen(false)
                                          }
                                      }
                                      className = {styles.select_user}
                                        
                                        />
            }
            {isCaseModalOpen && <SelectCase 
                                casesList={TypeFilteredCaseList}
                                isSelectCase = {isCaseModalOpen}
                                handleClose={() => setIsCaseModalOpen(false)}
                                handleSelect={(selectedCase: CaseProps) => {
                                    const game = tableGames![modalElement.id]
                                    //@ts-ignore
                                    dispatch(changeGame(request, {id: game.id, table: tableID, case: selectedCase.id, gameType: gameType}, gameType, 'add'))
                                    setIsCaseModalOpen(false)
                                }} className = {styles.select_user}           
                              />
            }



            <NewElementBtn 
                classNameProps={styles.add_empty_game} 
                onClick={() => {
                    // @ts-ignore
                    dispatch(addGame(request, tableID, gameType))
                }}
            />

            {pageType === 'admin'
                ? <Btn btnType='outlined' 
                   color = 'blue-main' 
                   width={400}
                   height={48}
                   className={styles.net__btn_submit}
                   onClick={() => {
                                    if (localStorage.getItem(askAgain) ==='no') {
                                      request('classes/send_games/', {method: 'GET'})
                                      .then(data => setIsSendNetModalOpenResponse('success'))
                                      .catch(error => setIsSendNetModalOpenResponse('failure'))
                                    } else {
                                      setIsSendNetModalOpen(true);
                                    }
                                  }}
                 >Отправить сетку</Btn>
                : null
            }


            {isSendNetModalOpen && <SendNetModal 
                                        isSendNet={isSendNetModalOpen} 
                                        handleClose={() => setIsSendNetModalOpen(false)}
                                        handleSend={() => {
                                            setIsSendNetModalOpen(false);
                                            request('classes/send_games/', {method: 'GET'})
                                                .then(data => setIsSendNetModalOpenResponse('success'))
                                                .catch(error => setIsSendNetModalOpenResponse('failure'))
                                        }}
                                    />
            }

            {isSendNetModalOpenResponse === 'success' && <SendNetModalSuccess
                                                            isSendNet={true}
                                                            handleClose={() => setIsSendNetModalOpenResponse('hide')}
                                                          />
            }
            {isSendNetModalOpenResponse === 'failure' && <SendNetModalError
                                                            errorMessage={error}
                                                            isSendNet={true}
                                                            handleClose={() => setIsSendNetModalOpenResponse('hide')}
                                                            handleSend={() => {
                                                                request('classes/send_games/', {method: 'GET'})
                                                                    .then(data => setIsSendNetModalOpenResponse('success'))
                                                                    .catch(error => setIsSendNetModalOpenResponse('failure'))
                                                            }}
                                                        />
            }

        </div>
    )
}

export default Net