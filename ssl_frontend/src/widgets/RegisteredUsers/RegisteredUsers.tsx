import { useState } from 'react';
import { Title, Text, Btn } from 'shared';
import { UsersList } from 'entites';
import { ParticipantProps } from 'shared/types';
import styles from './RegisteredUsers.module.scss';
import { AddParticipant } from 'features/AddParticipant';
import { addUnregisterUser } from 'app/actions';
import { useDispatch } from 'react-redux';
import useHttp from 'app/hooks/http';
import { CaseType } from 'shared/types/case';

interface Props {
    usersList: Array<ParticipantProps>
    notRegisterUsersList: Array<ParticipantProps>
    pageType: 'admin' | 'referee'
    gameType: CaseType
}


function RegisteredUsers(props: Props) {
    const {usersList,  notRegisterUsersList, pageType, gameType } = props

    const [isModalOpen, setIsModalOpen] = useState<boolean>(false);
    
    const dispatch = useDispatch();
    const {request} = useHttp(); 

    const usersLeft: number = usersList.reduce( 
        function (accumulyator: number, currentValue: ParticipantProps): number{
            if (gameType === 'conflicts'){
                return accumulyator + Number(currentValue.countConflictsGames === 0)
            } else {
                return accumulyator + Number(currentValue.countNegotiationsGames === 0)
            }
        }, 0
    );
    
    return (
        <div className={styles.users__wrapper}>
            <div className={styles.main_info}>
                <Title type='h3' font_style='bold' className={styles.m0_auto}>Игроки</Title>
                {usersLeft === 0 
                ? <Text type='paragraph' className={styles.m0_auto}>Все игроки добавлены в сетку!</Text> 
                : <Text type='paragraph' className={styles.m0_auto}>Осталось добавить в сетку {usersLeft} из {usersList.length}</Text>}
                {/* <Text>Осталось добавить в сетку `$`</Text> */}
            </div>
            <div className={styles.userslist__wrapper}>                
                <UsersList {...props} pageType={pageType} gameType={gameType} />
            </div>

            <Btn
                btnType='text'
                color = 'blue-main'
                onClick = {() => setIsModalOpen(true)}
                >Добавить</Btn>

            {isModalOpen && <AddParticipant
                notRegisterUsersList={notRegisterUsersList} 
                isSelectUser={isModalOpen}
                handleClose={() => setIsModalOpen(false)}
                handleSelect={(user: ParticipantProps) => {
                    //@ts-ignore
                    dispatch(addUnregisterUser(request, user));
                    setIsModalOpen(false);
                } } className={styles.select_user} comparator={'conflicts'}                                        />
            }
        </div>
    )
}

export default RegisteredUsers