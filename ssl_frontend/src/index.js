import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter as Router} from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './app/store';
import { Suspense } from 'react';
import App from 'app/App';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Suspense>
        <Provider store={store}>
            <Router>
                <App />
            </Router>
        </Provider>
    </Suspense>
);
