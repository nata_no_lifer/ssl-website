import basePureRequest from "./basePureRequest"

export default async function baseRequest(path: string, options: any) {
    let response = await basePureRequest(path, options)

    let result = await response.json()
    return {result: result, ok: response.ok}
}
