const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT;
export default async function basePureRequest(path: string, options: any) {

    const baseUrl = API_ENDPOINT;

    const token = "Bearer " +
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNzE4NzIyNDgyLCJpYXQiOjE3MDk2NTA0ODIsImp0aSI6IjBjMzczNDM1N2FkZTQyNTFiN2QyZWJjYWM3YzJmNmMxIiwidXNlcl9pZCI6MX0.FlsQWzhaSjnu1UZ-phNrX6_4DNIJk4PyAR_pXiXEvaA"

    let response = await fetch(baseUrl + path, {
        ...options,
        headers: {
            'Content-Type': 'application/json;charset=utf-8',
            'Authorization': token
        },
    })

    return response
}
