import React, {useRef} from "react";
import type { FC, MouseEvent } from "react";
import {CSSTransition} from "react-transition-group";
import classNames from "classnames";

import './Overlay.scss';
import { isAccessor } from "typescript";
import { node } from "webpack";

export interface OverlayProps {
    className?: string;
    isActive?: boolean;

    onClick?: (event: React.MouseEvent) => void;
    timeout?: number;
}

const Overlay: React.FC<OverlayProps> = ({
    className,
    isActive = false,
    onClick,
    timeout = 500,
    }) => {
    const nodeRef = useRef(null);
    return (

        <CSSTransition className={classNames("overlay", className)}
                                                 in={isActive} 
                                                 nodeRef={nodeRef} 
                                                 timeout={timeout} 
                                                 unmountOnExit 
                                                 onClick={onClick}>

        <div ref = {nodeRef}/>
        </CSSTransition>
    )
}

export default Overlay;