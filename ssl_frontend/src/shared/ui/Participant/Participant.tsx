import { useState } from 'react';
import {SquarePhoto} from 'shared';
import user_photo from '../../../assets/mock/user.jpg';
import close_icon from '../../../assets/icons/functional_icons/close.png';
import classNames from 'classnames';
import styles from './Participant.module.scss';
import { ParticipantProps } from 'shared/types';

interface Props {
    // Attributes
    participantType?: 'skeleton' | 'participant'
    participantData: ParticipantProps
    children?: JSX.Element | string
    handleDelete?: () => void
    gameCounter?: 'conflicts' | 'negotiations' | 'no'
    pageType?: "referee" | "admin"
    onStatusChange?: (status: "Не пришел" | "Опоздал") => void
    isCloseIcon?: boolean
}


const Participant = (props: Props) => {
    const [dropdown, setDropDown] = useState<boolean>(false);

    const {
        onStatusChange,
        participantType,  
        participantData,
        gameCounter = 'no',
        handleDelete = () => {},
        pageType,
        isCloseIcon = true,
    } = props;

    let participantTime: string = "-";

      if (participantData.arrivalTime === "16:30" && participantData.departureTime === "21:00") {
        participantTime = participantData.arrivalTime + " - " + participantData.departureTime;
      } else {
        if (participantData.departureTime !== "21:00"){
            participantTime = "до " + participantData.departureTime;
        } else if (!(participantData.arrivalTime === "16:30")){
            participantTime =  "c " + participantData.arrivalTime;
        }
      }

    const levelStyle = 'l_' + participantData.level;
    let participant!: JSX.Element

    if (participantType === 'skeleton') {
        participant = (
            <div className={styles.participant_skeleton__wrapper}>
                <button className={styles.btn__skeleton_add}>+</button>
            </div>
        )
    } else{
        participant = (
          <div className={classNames(styles.wrapper,
            {[styles.border_skipped]: participantData.attendance === 'Не пришел',
              [styles.border_delayed]: participantData.attendance === 'Опоздал',
              [styles.border_admin]: pageType === 'admin',
              [styles.border_referee]: pageType ==='referee'})}>
            <SquarePhoto
              type = 'participant'
              img={user_photo}
            />

            <div className={classNames(styles.participant__counter_hide,
              {[styles.participant__counter_show]: participantData.countConflictsGames >= 2 && gameCounter === 'conflicts'
               || participantData.countNegotiationsGames >= 2 && gameCounter === 'negotiations' 
               })}>Играет {participantData.countConflictsGames} раз </div>

          <div className={`${styles.participant__wrapper}`}>
              <div className={classNames(styles.status, 
                     {[styles.icon_skipped]: participantData.attendance === "Не пришел",
                     [styles.icon_delayed]: participantData.attendance === "Опоздал"})}></div>
              <div>
                  <p className={`${styles.name}`}>{participantData.firstName}</p>
                  <p className={`${styles.name}`}>{participantData.lastName}</p>
              </div>

              <div className={`${styles.info__wrapper}`}>
                  <div className={`${styles.level}`}>
        {/* TODO: Добавить цифру уровня */}
                      <div className={`
                                   ${styles[levelStyle]}`}/>
                      <p className={styles.level_style}>Уровень 1</p>
                  </div>
                   
                  <div className={`${styles.time}`}>
                      {participantTime}
                  </div>

              </div>
              {pageType === "admin" && isCloseIcon
              ? <button className={styles.close_icon} onClick={handleDelete}>
                  <img src={close_icon} alt="" />
                </button>
              : null}


              {pageType === "referee"
              ? <div className={styles.dropdown}>


                  <button className={classNames(styles.dropdown_icon, {[styles.dropdown_icon_rotate]: dropdown})} 
                  onClick = {() => {if (dropdown) { 
                                      setDropDown(false)
                                    } else {
                                      setDropDown(true)
                                    } 
                                  }}
                                                                              ></button>
                  {/* TODO: переделать drop-down */}
                  {dropdown && <div className={styles.dropdown_content}>
                                <button className={styles.dropdown_btn} onClick={() => {onStatusChange!("Не пришел"); setDropDown(false)}}>Не пришел</button>
                                <button className={styles.dropdown_btn} onClick= {() => {onStatusChange!("Опоздал"); setDropDown(false)}}>Опоздал</button>

                              </div>}

                </div>
                : null}

            </div>
        </div>
        )
    }
    return <>{participant}</>
}

export default Participant;
