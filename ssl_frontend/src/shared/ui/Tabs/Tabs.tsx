import React from "react";
import { memo } from "react";
import { ReactNode } from "react";
import ClassNames from "classnames";

import styles from './Tabs.module.scss';

export interface ITab {
    id: number
    label?: string | number
}

export interface TabsProps {
    className?: string;
    tabClassName?: string
    children?: ReactNode;
    tabs: ITab[];
    selectedId: number;
  
    // Events
    onClick: (id: number) => void;
}

const Tabs = memo((props: TabsProps) => {
    const {
      className = '',
      tabClassName = '',
      selectedId,
      tabs = []
    } = props

    const { onClick } = props
    return (

        <div className={`${className} ${styles.tab__wrapper}`}>
            {tabs && tabs.map(tab => (
                <div 
                    className={`${tabClassName} ${ClassNames(styles.tab__label, {
                        [styles.tab__label_selected]: tab.id === selectedId})}`}
                    key={tab.id}
                    onClick={() => onClick(tab.id)}
                         >
                    {tab.label}
                </div>
            ))}
          {props.children}
        </div>
    )
  })

export default Tabs;
