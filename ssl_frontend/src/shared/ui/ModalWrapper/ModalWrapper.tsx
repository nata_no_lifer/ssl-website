import { ReactNode, memo } from 'react';

import close_icon from '../../../assets/icons/functional_icons/close.png';

import styles from './ModalWrapper.module.scss';

interface Props {
    isModalOpen: boolean
    showCloseBtn?: boolean
    closeModal: () => void
    children: ReactNode
    wrapperClass?: string
}

const ModalWrapper = memo((props: Props) => {
    const { isModalOpen, showCloseBtn = true, closeModal, wrapperClass } = props
    return (
      <div className={styles.modal__wrapper}>
        {isModalOpen ? (
          <div className={styles.overlay}>
            <div className={`${styles.modal} ${wrapperClass}`}>
              {showCloseBtn ? (
                <button onClick={closeModal} className={styles.close}>
                  <img src={close_icon} alt="" className={styles.close_icon}/>
                </button>
              ) : null}
              {props.children}
            </div>
          </div>
        ) : null}
      </div>
    )
  })
  
  export default ModalWrapper
  
  