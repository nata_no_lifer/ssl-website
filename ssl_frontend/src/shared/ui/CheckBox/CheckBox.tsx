import { memo, ReactNode } from 'react';
import { useState } from 'react';
import styles from './CheckBox.module.scss';

interface Props {
  // Attributes
  // btnType?: 'tonal' | 'outlined' | 'text' | 'button'
    type?: 'checkbox'
    width?: number | string
    height?: number | string
    color?:
    | 'blue-main'
    | 'black-main'
    | 'secondary-black'
    | 'grey'
    className?: string
    children?: ReactNode
    check?: boolean
    //   children?: JSX.Element| string

    // Events
    onClick?: () => void
}


const CheckBox = memo((props: Props) => {
    const [agreement, setAgreement] = useState(false);

    const handleChange = (event: any) => {
        setAgreement(event.target.checked);
    }

    const {
        type = 'checkbox',
        check,
        className = '',
        color = 'black-main'
    } = props
    const { onClick } = props

    return (
        // className={styles.checkbox}
        <div className={`${className} ${styles.checkbox} ${styles[color]}`}>
            <input
                // value={value}
                // className="btn outlined"
                type={type}
                onChange={handleChange}
                onClick={onClick}
            /> 
            <span>{props.children}</span>
        </div>
    )
})

export default CheckBox
