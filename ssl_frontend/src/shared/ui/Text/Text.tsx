import { ReactNode } from 'react'
import styles from './Text.module.scss'

interface Props {
  type: 'paragraph' | 'small'
  color?:
    | 'white-main'
    | 'black-main'
    | 'grey'
  weight?: 'regular' | 'bold'
  className?: string
  onClick?: () => void
  children?: ReactNode
}

const Text = (props: Props) => {
  const {
    type,
    color = 'black-main',
    weight = 'regular',
    className = '',
    onClick,
  } = props
  return (
    <>
      <p
        className={`${styles[type]} ${styles[color]} ${styles[weight]} ${className}`}
        onClick={onClick}
      >
        {props.children}
      </p>
    </>
  )
}

export default Text
