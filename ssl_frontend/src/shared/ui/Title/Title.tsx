import { ReactNode } from 'react'
import styles from './Title.module.scss'

interface Props {
  type?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6'
  color?: 'black-main'
  margin?: 'mb_40' | 'mb_24' | 'm0_auto'
  font_style?: 'bold'
  className?: string
  children?: ReactNode
}

const Title = (props: Props) => {
  const { type, color = 'black-main', className = '', margin = '', font_style = '' } = props
  let header!: JSX.Element

  if (type === 'h1' || type === 'h2') {
    header = (
      <h2 className={`${className} ${styles[type]} ${styles[color]} ${styles[margin]} ${styles[font_style]}`}>
        {props.children}
      </h2>
    )
  } else if (type === 'h3') {
    header = (
      <h3 className={`${className} ${styles[type]} ${styles[color]} ${styles[font_style]}`}>
        {props.children}
      </h3>
    )
  } else if (type === 'h4') {
    header = (
      <h4 className={`${className} ${styles[type]} ${styles[color]}`}>
        {props.children}
      </h4>
    )
  }

  return <>{header}</>
}

export default Title
