import { ReactNode } from 'react'
import styles from './TextInput.module.scss'


interface Props {
  name?: string
  className?: string
  children?: ReactNode
  placeholder?: string
  // children: JSX.Element | JSX.Element[] | string
}

const TextInput = (props: Props) => {
  const {name, className = '', children, placeholder } = props
  // const [field, meta] = useField(props);
  return (
    <>
      <label htmlFor={name}>{children}</label>
      <input placeholder={placeholder} className={className} name={name} />
      {/* {meta.touched && meta.error ? (
        <div className="error">{meta.error}</div>
      ) : null} */}
    </>
  );
};

export default TextInput
