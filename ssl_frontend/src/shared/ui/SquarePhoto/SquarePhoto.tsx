import { memo } from 'react'
import { Img } from 'react-image'
import styles from './SquarePhoto.module.scss'

interface Props {
  img: string
  type: 'participant' | 'referee'
  className?: string
}

const SquarePhoto = memo((props: Props) => {
  const { img, type, className } = props
// TODO: Добавить loader для фото
  return (
    <>
      <Img
        className={`${styles.photo} 
                    ${className}
                    ${styles[type]}`}
        src={img}
        loader={
          <div
            className={`${styles.skeleton__img} 
                        ${className} 
                        ${styles[type]}`}
          ></div>
        }
        // unloader={}
      />
    </>
  )
})

export default SquarePhoto
