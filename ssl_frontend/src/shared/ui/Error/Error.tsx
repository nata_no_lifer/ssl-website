import { memo } from 'react';

import exclamation_mark from '../../../assets/icons/functional_icons/exclamation_mark.svg';

import styles from './Error.module.scss';

interface Props {
    children?: JSX.Element
    textChildren?: string
    zindex?: number
    isError?: boolean
    borderRadius?: number
    left1?: number
}

const Error = (props: Props) => {
    const {
        textChildren,
        zindex=1,
        isError=true,
        borderRadius: radius = 24,
        left1: left1 = 82
      } = props

    if (isError) {
        return (
            <div className={`${styles.error_outlined}`} style={{borderRadius: radius + 'px'}}>
                <div className={`${styles.error__wrapper}`}>

                    <img src={exclamation_mark} alt="" className={styles.error__icon} style = {{zIndex: zindex +1 }}/>
                    <div className={styles.test}></div>
                    <div className={`${styles.error__text}`}
                         style = {{zIndex: zindex, left: left1 + '%'}}>{props.textChildren}</div>
                </div>
                {props.children}
            </div>
        )
    }

    return (
        <>
        {props.children}
        </>
)
  }
  
  export default Error
  

  