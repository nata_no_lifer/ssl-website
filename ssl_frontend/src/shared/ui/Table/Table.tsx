import React from 'react';
import ClassNames from "classnames";
import close_icon from '../../../assets/icons/functional_icons/close.png';
import { useState } from 'react';
import styles from './Table.module.scss';


interface Props {
    tableName?: string
}

const Table = (props: Props) => {

  const [Table, setTable] = useState(false);

    const {
        tableName = ''
    } = props
  return (

    <div className={styles.table__wrapper}>
        <p className={`${styles.name}`}>{tableName}</p>
        <button onClick={() => setTable(true)} className={styles.close_icon}>
          <img src={close_icon} alt="" />
        </button>
    </div>
  )
}

export default Table
