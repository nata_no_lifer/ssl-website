import { useState, useEffect } from 'react';
import classNames from 'classnames';
import styles from './NewTextInput.module.scss';

function NewTextInput(props: any) {

    const {name, label, startValue, setCurrentValue, error, setError} = props;
    const [value, setValue] = useState(startValue);


    useEffect(() => {
        setCurrentValue(value);
    }, [value]);


    return (
        <div className={styles.input_container}>
            <input 
                type="text"
                name={name}
                id={name}
                placeholder=" "
                className={classNames(styles.input, (error ? styles.input_error : styles.input_normal))}
                value={value}
                onChange={(e) => {
                        setValue(e.target.value);
                        setError('');
                    }
                }
            />
            <label htmlFor={name} className={styles.placeholder}>{label}</label>
            <div className={styles.error}>{error}</div>
        </div>
    )
}

export default NewTextInput;