import { memo } from 'react'
import styles from './Btn.module.scss'
//не импортировалось так, поэтому сделала через require 
// const styles = require('Btn/Btn.scss');
interface Props {
  // Attributes
  btnType?: 'tonal' | 'outlined' | 'text' | 'button' | 'test'
  type?: 'button' | 'submit' | 'reset'
  width?: number | string
  height?: number | string
  color?:
    | 'blue-main'
    | 'black-main'
    | 'secondary-black'
    | 'grey'
  className?: string
  children?: JSX.Element | string

  // Events
  onClick?: () => void
}

const Btn = memo((props: Props) => {
  const {
    btnType = 'tonal',
    width,
    height,
    type = 'button',
    className = '',
    color = 'purple-main',
  } = props
  const { onClick } = props


  let inlineWidth: string
  if (typeof width === 'string') {
    inlineWidth = width
  } else {
    inlineWidth = `${width}px`
  }

  let inlineHeight: string
  if (typeof height === 'string') {
    inlineHeight = height
  } else {
    inlineHeight = `${height}px`
  }


  return (
    <>
      <button
        style={{ width: inlineWidth, height: inlineHeight}}
        className={`${className} 
                  ${styles.btn} 
                  ${styles[btnType]} 
                  ${styles[color]} `}
        type={type}
        onClick={onClick}
      >
        {props.children}
      </button>
    </>
  )
})

export default Btn
