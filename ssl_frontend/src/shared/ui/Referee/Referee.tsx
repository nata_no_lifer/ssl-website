import { memo } from 'react';
import SquarePhoto from '../SquarePhoto/SquarePhoto';
import user from '../../../assets/mock/user.jpg';
import close_icon from '../../../assets/icons/functional_icons/close.png';
import styles from './Referee.module.scss';

interface Props {
  // Attributes
  className?: string
  children?: JSX.Element | string
  first_name?: string
  last_name?: string
  refereeType?: 'skeleton' | 'referee'

  // Events
  handleDelete?: () => void
}

const Referee = (props: Props) => {
  const {
    className = '',
    first_name = '',
    last_name = '',
    handleDelete
  } = props

  return (

      <div className={`${className} 
                    ${styles.referee__wrapper}`}>
              <button className={styles.btn_close} onClick={handleDelete}>
                <img src={close_icon} alt="close icon"  />
              </button>
              <SquarePhoto
                type = 'referee'
                img={user}
                // className={styles.squarePhoto}
              />
              <div className={`$styles.name__wrapper`}>
                  <p className={`${styles.name}`}>{first_name}</p>
                  <p className={`${styles.name}`}>{last_name}</p>
              </div>
      </div>
  )
}

export default Referee
