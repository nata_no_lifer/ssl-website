import { memo } from 'react';
import close_icon from '../../../assets/icons/functional_icons/close.png';
import styles from './Case.module.scss';

interface Props {
  // Attributes
  className?: string
  children?: JSX.Element | string
  btn_close: 'show' | 'hide'
  handleDelete?: () => void
  // Events
  onClick?: () => void
}

const Case = memo((props: Props) => {
  const {
    className = '',
    btn_close = 'show',
    handleDelete
  } = props


  return (
    <div className={`${className} 
                  ${styles.case__wrapper}`}>
        {btn_close === 'show' ? <button className={`${styles.close_icon}`} onClick = {handleDelete}>
                                    <img src={close_icon} alt="" />
                                  </button>
                              : null}

        <div className={styles.main_wrapper}>
          <p className={`
                    ${styles.case}`}>Кейс</p>
          <span className={`
                    ${styles.case_number}`}>№ {props.children}</span>
        </div>


    </div>
  )
})

export default Case
