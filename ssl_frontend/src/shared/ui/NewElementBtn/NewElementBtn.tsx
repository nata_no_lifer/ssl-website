import React from 'react';
import add from 'assets/icons/functional_icons/add.svg';
import styles from './NewElementBtn.module.scss';

interface Props {
    classNameProps?: string,
    onClick?: () => void
}


const NewElementBtn = (props: Props) => {
    const {
        classNameProps = '',
        onClick = () => {}
    } = props

    return (
        <div>
          <div className={`${styles.participant_skeleton__wrapper} ${classNameProps}`}
               onClick = {onClick}>
            <button className={styles.btn__skeleton_add}>
                <img src={add} alt="add Element" />
            </button>
          </div>
        </div>
    )
}

export default NewElementBtn
