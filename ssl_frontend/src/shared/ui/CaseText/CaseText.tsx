import React, {useState} from 'react';
import {Title, Text} from 'shared';

import { CaseProps } from 'shared/types';

import styles from './CaseText.module.scss';


interface Props {
    caseData: CaseProps
    children?: JSX.Element | string
    className?: string
};

const CaseText  = (props: Props) => {
    // const CaseText  = () => {
    const { 
        caseData
      } = props;

    return (
        <div className={styles.case_text__wrapper}>
            <p className={styles.case_title}>{caseData.title}</p>
            <p className={styles.case_text}>{caseData.text}</p>
            <div className={styles.case_text__tags}>
                {caseData.tags.map((tag) => {
                    return <div className={styles.case_type}>{tag}</div>
                })}
            </div>

            {/* <button className={styles.case_btn}>Показать весь текст</button> */}
        </div>
    )

}

export default CaseText;
