const routes = {
		regPage: '/registration',
		authPage: '/login',

		mainPage: '/',
		nextGamePage: '/next',
		pastGamePage: '/past',
		rulesPage: '/rules',
		ratingPage: '/rating',
		userProfilePage: '/profile',

		adminNetPage: '/classes',
		refereeNetPage: '/referee/net',
		allTablesPage: '/classes/tables'
}

const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT;
const url = API_ENDPOINT;

export default routes;
export { url };