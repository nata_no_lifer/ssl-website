import Btn from './ui/Btn/Btn';
import SquarePhoto from './ui/SquarePhoto/SquarePhoto'
import Title from './ui/Title/Title';
import Text from './ui/Text/Text';
import TextInput from './ui/TextInput/TextInput';
import NewTextInput from './ui/NewTextInput/NewTextInput';
import CheckBox from './ui/CheckBox/CheckBox';
import Tabs from './ui/Tabs/Tabs';
import Table from './ui/Table/Table';
import Overlay from './ui/Overlay/Overlay';
import Case from './ui/Case/Case';
import Participant from './ui/Participant/Participant';
import Referee from './ui/Referee/Referee';
import ModalWrapper from './ui/ModalWrapper/ModalWrapper';
import NewElementBtn from './ui/NewElementBtn/NewElementBtn';
import CaseText from './ui/CaseText/CaseText';
import BaseRequest from './api/baseRequest'
import BasePureRequest from './api/basePureRequest'
// import Error from './ui/Error/Error';

import routes from './const/routes';
import { url } from './const/routes';
import { _apiUrl } from './const/index';
import askAgain from './const/localStorage';

import {
    TStatusType
  } from './types/common'

// import InputField from './ui/InputField/InputField';
export {
    Btn,
    SquarePhoto,
    Title,
    Text,
    TextInput,
    CheckBox,
    Tabs,
    Table,
    Overlay,
    Case,
    Participant,
    Referee,
    ModalWrapper,
    NewElementBtn,
    CaseText,
    routes,
    url,
    _apiUrl,
    BaseRequest,
    BasePureRequest,
    NewTextInput,
    // Error,
    askAgain
    // InputField
};

export type {
    TStatusType
}