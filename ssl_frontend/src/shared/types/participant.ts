import { Role } from "./user"

export interface ParticipantProps {
    id: number
    firstName: string
    lastName: string
    level: number
    img: string
    arrivalTime: string
    departureTime: string
    countConflictsGames: number
    countNegotiationsGames: number
    role: Role
    attendance?: "Не пришел" | "Опоздал",
    allTime: boolean
}

export type ParticipantID = number | null
