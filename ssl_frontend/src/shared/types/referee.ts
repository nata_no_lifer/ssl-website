import { ParticipantProps } from "./participant";

export interface RefereeProps extends ParticipantProps {
    refereeId: number
}
