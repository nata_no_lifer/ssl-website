export interface CaseProps {
    id: number
    number: number
    tags: Array<string>
    title: string
    text: string
    type: CaseType
}

export type CaseID = number | null

export type CaseTypeRu = 'Конфликт' | 'Переговоры'

export type CaseType = 'conflicts' | 'negotiations'
