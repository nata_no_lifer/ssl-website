export interface IPhoto {
    id: number
    time_create: string
    // uri строка
    image: string
  }
  
  export type TStatusType = 'idle' | 'loading' | 'success' | 'error'
  