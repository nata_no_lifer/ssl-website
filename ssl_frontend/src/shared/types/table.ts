import { IGameInfo, GameInfoBD } from "./game"
import { RefereeProps } from "./referee"

export interface TableBD {
    id: number
    tableName: string
    referees: { [id: number]: number }
    negotiations: Array<GameInfoBD>
    conflicts: Array<GameInfoBD>
}

export interface TableProps {
    id: number
    tableName: string
    referees: Array<RefereeProps>
    negotiations: Array<IGameInfo>
    conflicts: Array<IGameInfo>
}

export type GameType = 'negotiations' | 'conflicts'
