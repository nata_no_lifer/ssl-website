export type Role = 'user' | 'student' | 'arbitrator' | 'admin'

export interface UserProps {
    id: number
    firstName: string
    lastName: string
    fatherName: string
    telegram: string
    img: string | null
    email: string
    hsePass: boolean
    isAccepted: boolean
    isVerifiedEmail: boolean
    role: Role
    level: number
}
