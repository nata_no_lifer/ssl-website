import { ParticipantID, ParticipantProps } from "./participant"
import { CaseID, CaseProps } from "./case"

export interface IGameInfo {
    id: number
    participant1: ParticipantProps | null
    participant2: ParticipantProps | null
    case: CaseProps | null
    player1Score: number
    player2Score: number
}

export interface GameInfoBD {
    id: number
    participant1: ParticipantID
    participant2: ParticipantID
    case: CaseID | null
    player1Score: number
    player2Score: number
}
