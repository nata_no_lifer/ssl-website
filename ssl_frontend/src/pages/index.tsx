import { lazy } from 'react';
import { Route, Routes } from 'react-router-dom';
import { routes } from 'shared';


const RegPage = lazy(() => import('./RegPage/RegPage')),
	  AuthPage = lazy(() => import('./AuthPage/AuthPage'));

const MainPage = lazy(() => import('./MainPage/MainPage')),
	  NextGamePage = lazy(() => import('./NextGamePage/NextGamePage')),
	  PastGamePage = lazy(() => import('./PastGamePage/PastGamePage')),
	  RulesPage = lazy(() => import('./RulesPage/RulesPage')),
	  RatingPage = lazy(() => import('./RatingPage/RatingPage')),
	  UserProfilePage = lazy(() => import('./UserProfilePage/UserProfilePage'));

const AdminNetPage = lazy(() => import('./AdminNetPage/AdminNetPage')),
	  AllTablesPage = lazy(() => import('./AllTablesPage/AllTablesPage'));
// const RefereeNetPage = lazy(() => import('./RefereeNetPage/RefereeNetPage'));


export const Routing = () => {
	return (
		<Routes>
        	<Route path={routes.regPage} element={<RegPage/>} />
			<Route path={routes.authPage} element={<AuthPage/>} />

			<Route path={routes.mainPage} element={<MainPage/>} />
			<Route path={routes.nextGamePage} element={<NextGamePage/>} />
			<Route path={routes.pastGamePage} element={<PastGamePage/>} />
			<Route path={routes.rulesPage} element={<RulesPage/>} />
			<Route path={routes.ratingPage} element={<RatingPage/>} />
			<Route path={routes.userProfilePage} element={<UserProfilePage/>} />
	
			<Route path={routes.adminNetPage} element={<AdminNetPage pageType={'referee'}/>} />
			{/* <Route path={routes.refereeNetPage} element={<RefereeNetPage/>} /> */}
			<Route path={routes.allTablesPage} element={<AllTablesPage pageType={'referee'}/>} />
		</Routes>
	)
}


export {
    AdminNetPage,
    MainPage
}