import { useState, useEffect } from 'react';
import { Header, NewFooter } from 'widgets';
import { Navigate, NavLink } from "react-router-dom";
import { routes, url } from "shared";
import Cookies from 'js-cookie';

import styles from './RatingPage.module.scss';

function RatingPage() {

    const [isAuth, setIsAuth] = useState(Cookies.get('token') ? true : false);

    function RedirectHandler() {
        if (!isAuth) {
            return <Navigate to={routes.authPage} replace={true} />
        } else {
            return <></>
        }
    }


    const loadUser = async () => {
        let token = `Bearer ${Cookies.get('token')}`;

        const response = await fetch(`${url}users/profile/${Cookies.get('id')}/`, {
            method: 'GET',
            headers: {
                Authorization: token
            }
        });

        const json = await response.json();
        console.log(json);
    }


    useEffect(() => {
        document.title = 'Рейтинг';
    }, []);


    return (
        <>
            <RedirectHandler />

            <Header userRole={1} />

            <NewFooter />
        </>
    )
}

export default RatingPage;