import { useEffect } from 'react';
import { NewRegistrationForm } from 'features/NewRegistrationForm';
import { AuthSwitcher, FormGreyBackground } from 'widgets';
import styles from './RegPage.module.scss';


function RegPage() {
    useEffect(() => {
        document.title = 'Регистрация';
    }, []);

    return (
        <FormGreyBackground>
            <div className={styles.form_wrapper}>
                <AuthSwitcher registration={true} />
                <NewRegistrationForm />
            </div>
        </FormGreyBackground>
    )
}

export default RegPage;