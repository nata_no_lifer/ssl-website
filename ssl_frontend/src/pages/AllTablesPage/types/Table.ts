import { CaseTypeRu } from "shared/types/case";

export default interface TableRequest {
    case_type: CaseTypeRu,
    name: string,
    text: string,
    number: number,
    label: Array<string>
}
