import { useEffect } from "react";
import { NewLoginForm } from "features/NewLogInForm";
import { AuthSwitcher, FormGreyBackground } from "widgets";
import styles from './AuthPage.module.scss';


function AuthPage() {
    useEffect(() => {
        document.title = 'Авторизация';
    }, []);

    return (
        <FormGreyBackground>
            <div className={styles.form_wrapper}>
                <AuthSwitcher login={true} />
                <NewLoginForm />
            </div>
        </FormGreyBackground>
    )
}

export default AuthPage;