import React, { useState, useEffect } from 'react';
import { ModalWrapper } from 'shared';

const Modal: React.FC = () => {
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    setShowModal(true);
    const timer = setTimeout(() => {
      setShowModal(false);
    }, 5000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <ModalWrapper isModalOpen={false} closeModal={function (): void {
          throw new Error('Function not implemented.');
      } } >
      {showModal && <div className="modal">Модальное окно</div>}
    </ModalWrapper>
  );
};

export default Modal;