import { useEffect, useState } from "react";
import { Net, Table, RegisteredUsers } from 'widgets';
import { ParticipantProps, TableProps, CaseProps } from "shared/types";
import useHttp from '../../app//hooks/http';
import styles from './AdminNetPage.module.scss';
import { CaseType } from "shared/types/case";
import { useDispatch, useSelector } from "react-redux";
import { fetchCases, fetchParticipants, fetchTables, fetchUsers } from "app/actions";
import { createSelector } from "@reduxjs/toolkit";
import { TableBD } from "shared/types/table";
import { RootState } from "app/store";
import { IGameInfo, GameInfoBD } from "shared/types/game";
import { RefereeProps } from "shared/types/referee";

interface Props {
    pageType: 'referee' | 'admin'
}


function AdminNetPage(props: Props) {
    const { pageType } = props

    const casesList: Array<CaseProps> = useSelector((state: RootState): Array<CaseProps> => Object.values(state.cases.casesMap))

    const participantList: Array<ParticipantProps> = useSelector((state: RootState): Array<ParticipantProps> => Object.values(state.participants.participantMap))
   
    const userList: Array<ParticipantProps> = useSelector((state: RootState): Array<ParticipantProps> => Object.values(state.users.userMap))


    const dispatch = useDispatch();
    const { request } = useHttp();

    const tableSelector = createSelector(
        (state: RootState): {[id: number]: ParticipantProps} => state.participants.participantMap,
        (state: RootState): {[id: number]: CaseProps} => state.cases.casesMap,
        (state: RootState): Array<TableBD> => state.tables.tables,
        (participants: {[id: number]: ParticipantProps}, cases: {[id: number]: CaseProps}, tables: Array<TableBD>): Array<TableProps> => 
            tables.map((table: TableBD): TableProps => ({
                id: table.id,
                tableName: table.tableName,
                referees: Object.entries(table.referees).map(([referee, register]: [string, number]): RefereeProps => ({...participants[register], refereeId: Number(referee)})),
                negotiations: table.negotiations.map((game: GameInfoBD): IGameInfo => ({
                    id: game.id,
                    participant1: game.participant1 === null || !participants[game.participant1] ? null : participants[game.participant1]!,
                    participant2: game.participant2 === null || !participants[game.participant2] ? null : participants[game.participant2]!,
                    case: game.case === null || !cases[game.case] ? null : cases[game.case]!,
                    player1Score: game.player1Score ? game.player1Score: 0,
                    player2Score: game.player2Score ? game.player2Score: 0,
                })),
                conflicts: table.conflicts.map((game: GameInfoBD): IGameInfo => ({
                    id: game.id,
                    participant1: game.participant1 === null || !participants[game.participant1] ? null : participants[game.participant1]!,
                    participant2: game.participant2 === null || !participants[game.participant2] ? null : participants[game.participant2]!,
                    case: game.case === null || !cases[game.case] ? null : cases[game.case]!,
                    player1Score: game.player1Score ? game.player1Score: 0,
                    player2Score: game.player2Score ? game.player2Score: 0,
                }))
            }))
    );
    const tableList: Array<TableProps> = useSelector(tableSelector)

    const [selectedTableId, setSelectedTableId] = useState<number>(0);

    // состояние для стола конфликты/переговоры
    const [gameType, setGameType] = useState<CaseType>('conflicts');
    const participantsLoadingStatus = useSelector((state: RootState) => state.participants.participantsLoadingStatus);


    useEffect(() => {
        //@ts-ignore
        dispatch(fetchTables(request));

        //@ts-ignore
        dispatch(fetchParticipants(request));

        //@ts-ignore
        dispatch(fetchUsers(request));

        //@ts-ignore
        dispatch(fetchCases(request));
    }, []);


    if (tableList.length === 0) {
        return null;
    }


    if (participantsLoadingStatus === "loading") {
        return <h5>загрузка</h5>;
    } else if (participantsLoadingStatus === "error") {
        return <h5>Ошибка загрузки</h5>
    }

    return (
        <div className={styles.wrapper}>
            <div className={styles.left_column}>
                <Table
                    participantsList={participantList}
                    tableList = {tableList}
                    selectedTableId = {selectedTableId}
                    setSelectedTableId = {setSelectedTableId}
                    gameType = {gameType}
                    setGameType = {setGameType}
                />

                <Net
                    tableID={tableList[selectedTableId].id}
                    gameType={gameType}
                    tableGames={tableList[selectedTableId][gameType]}
                    usersList={participantList}
                    casesList={casesList} 
                    pageType={pageType}/>

            </div>
            <RegisteredUsers
                    pageType={pageType}
                    usersList={participantList}
                    notRegisterUsersList={userList}
                    gameType={gameType}
            />
        </div>
    )
}

export default AdminNetPage
