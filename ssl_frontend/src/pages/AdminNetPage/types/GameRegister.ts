import{StatusType} from "../lib/Status2Level";
import Profile from "./Profile";

export default interface GameRegister {
    id: number,
    date: string,
    begin_time: string,
    finish_time: string,
    player: Profile,
    attendance: string,
    count_games: number
}
