import { CaseTypeRu } from "shared/types/case"

export default interface CaseRequest {
    case_type: CaseTypeRu
    name: string,
    text: string,
    number: number,
    label: Array<string>
}
