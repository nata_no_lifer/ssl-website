import { useState, useEffect } from 'react';
import { Header, NewFooter } from 'widgets';
import NextGame from 'features/NextGame';
import { Navigate, NavLink } from "react-router-dom";
import { routes, url } from "shared";
import Cookies from 'js-cookie';
import closeButton from 'assets/icons/functional_icons/close1.png';
import { ModalNextGame } from 'features/modals';
import styles from './MainPage.module.scss';


function MainPage() {

    const [isAuth, setIsAuth] = useState(Cookies.get('token') ? true : false);

	const [step, setStep] = useState(0);

    const [isModal, setIsModal] = useState(false),
          [modalType, setModalType] = useState('');


    function RedirectHandler() {
        if (!isAuth) {
            return <Navigate to={routes.authPage} replace={true} />
        } else {
            return <></>
        }
    }


    const loadUser = async () => {
        let token = `Bearer ${Cookies.get('token')}`;

        const response = await fetch(`${url}/api/users/profile/${Cookies.get('id')}/`, {
            method: 'GET',
            headers: {
                Authorization: token
            }
        });

        const json = await response.json();
        console.log(json);
    }


    useEffect(() => {
        document.title = 'Главная';
    }, []);


    const saveNextGame = () => {
        setIsModal(false);
		setModalType('');
    };


    const setModalContent = () => {
        switch(modalType){
            case '':
                return (<></>);
            case 'NextGame':
                return (
                    <>
                        <ModalNextGame setsModal={{
                            openModal: () => {
                                setIsModal(true);
                                setModalType('NextGame');
                            },
                            closeModal: () => {
                                setIsModal(false);
                                setModalType('');
                            },
                            saveModal: () => {
                                setIsModal(false);
                                setModalType('');
                                setStep(1);
                            }
                        }} />
                    </>
                );
        }
    };


    const modal = () => {
		if (isModal) {
			return(
				<div className={styles.modal}>
					<div className={styles.window}>
                        <div className={styles.head}>
                            <div className={styles.title}>{'Запись на игру'}</div>
                            <div className={styles.close} onClick={() => {
                                setIsModal(false);
                                setModalType('');
                            }}>
                                <img src={closeButton} alt="close" />
                            </div>
                        </div>
                        {setModalContent()}
					</div>
				</div>
			);
		} else {
			return (<></>);
		}
	}


    return (
        <>
            <RedirectHandler />
            {modal()}

            <Header userRole={1} />

            <NextGame setsModal={{
                openModal: () => {
                    setIsModal(true);
                    setModalType('NextGame');
                },
                closeModal: () => {
                    setIsModal(false);
                    setModalType('');
                }, 
                step: step
            }} />

            <section className={styles.links}>
                <NavLink to={routes.rulesPage}>
                    <div className={styles.link}>
                        Правила
                    </div>
                </NavLink>
                
                <NavLink to={routes.ratingPage}>
                    <div className={styles.link}>
                        Рейтинги
                    </div>
                </NavLink>
            </section>

            <NewFooter />
        </>
    )
}

export default MainPage;