import { useEffect, useState } from "react";
import { Navigate } from "react-router-dom";
import Cookies from "js-cookie";
import { Header, NewFooter } from "widgets";
import exitImg from '../../assets/icons/functional_icons/userExit.svg';
import { routes, url } from "shared";
import { ModalData, ModalEmail, ModalPassword } from "features/modals";
import closeButton from 'assets/icons/functional_icons/close1.png';

import styles from './UserProfilePage.module.scss';
import '../../shared/styles/wrapper.scss';


function UserProfilePage() {  
    const [userName, setUserName] = useState(''),
          [userSurname, setUserSurname] = useState(''),
          [userFathername, setUserFathermame] = useState(''),
          [userTelegram, setUserTelegram] = useState(''),
          [userEmail, setUserEmail] = useState(''),
          [userImage, setUserImage] = useState(''),
          [userRating, setUserRating] = useState(''),
          [userPassword, setUserPassword] = useState(''),
          [userRepeatedPassword, setUserRepeatedPassword] = useState('');

    const [isAuth, setIsAuth] = useState(Cookies.get('token') ? true : false);

    const [isModalActive, setIsModalActive] = useState(false),
          [modalType, setModalType] = useState('');


    const loadUser = async () => {
        let token = `Bearer ${Cookies.get('token')}`;

        const response = await fetch(`${url}/users/profile/${Cookies.get('id')}/`, {
            method: 'GET',
            headers: {
                Authorization: token
            }
        });

        const json = await response.json();
        console.log(json);

        if (response.ok) {
            setUserName(json.first_name);
            setUserSurname(json.last_name);
            setUserFathermame(json.father_name);
            setUserTelegram(json.telegram);
            setUserEmail(json.email);
            setUserImage(url + json.image);
            setUserRating(json.rating);
        } else {
            console.log(response.status);
        }
    }


    useEffect(() => {
        document.title = 'Личный кабинет';
    }, []);


    useEffect(() => {
        loadUser();
    }, [])


    const logoutUser = () => {
        // let token = `Bearer ${Cookies.get('token')}`;

        // const response = await fetch(`${domain}users/auth/logout/` ,{
        //     method: 'POST',
        //     headers: {
        //         Authorization: token
        //     }
        // });

        // const result = response.json();

        // if (response.ok) {
        //     Cookies.remove('token');
        // } else {
        //     console.log(response);
        // }

        Cookies.remove('token');
        setIsAuth(false);
    }


    function RedirectHandler() {
        if (!isAuth) {
            return <Navigate to={routes.authPage} replace={true} />
        } else {
            return (<></>)
        }
    }


    function Modal() {
        const setTitle = () => {
            switch(modalType) {
                case 'data':
                    return (
                        'Редактрирование личных данных'
                    )
                case 'email':
                    return (
                        'Редактирование почты'
                    )
                case 'password':
                    return (
                        'Редактирование пароля'
                    )
                case '':
                    return (null)
            }
        }


        const closeModal = () => {
            setIsModalActive(false);
            setModalType('');
        }


        const setModal = () => {
            switch(modalType) {
                case 'data':
                    return (
                        <ModalData close={() => closeModal()} />
                    )
                case 'email':
                    return (
                        <ModalEmail close={() => closeModal()} />
                    )
                case 'password':
                    return (
                        <ModalPassword close={() => closeModal()} />
                    )
                case '':
                    return (null)
            }
        }


        if (isModalActive) {
            return (
                <div className={styles.modal}>
                    <div className={styles.window}>
                        <div className={styles.head}>
                            <div className={styles.title}>{setTitle()}</div>
                            <div className={styles.close} onClick={() => {
                                setIsModalActive(false);
                                setModalType('');
                            }}>
                                <img src={closeButton} alt="close" />
                            </div>
                        </div>
                        {setModal()}
                    </div>
                </div>
            );
        } else {
            return (<></>);
        }
    }
    

    return (
        <>
            <RedirectHandler />

            {Modal()}

            <Header userRole={1} image={userImage} />

            <section className={styles.profile_content}>
                <div className={styles.left}>
                    <div className={styles.data}>
                        <div className={styles.photo}>
                            <img src={userImage} alt="User" />
                        </div>

                        <div className={styles.info}>
                            <div className={styles.name}>{userSurname}</div>
                            <div className={styles.name}>{userName}</div>
                            <div className={styles.name}>{userFathername}</div>

                            <div className={styles.extra}>
                                <div className={styles.rating}>{`${userRating} уровень`}</div>
                                <div className={styles.telegram}>{userTelegram}</div>
                            </div>
                        </div>
                    </div>

                    <div className={styles.change} onClick={() => {
                        setIsModalActive(true);
                        setModalType('data');
                    }}>
                        Изменить личные данные
                    </div>
                </div>

                <div className={styles.right}>
                    <div className={styles.settings}>
                        <div className={styles.setting_block}>
                            <div className={styles.setting_text}>{userEmail}</div>
                            <div className={styles.setting_set} onClick={() => {
                                setIsModalActive(true);
                                setModalType('email');
                            }}>
                                Изменить почту
                            </div>
                        </div>

                        <div className={styles.setting_block}>
                            <div className={styles.setting_text}>********</div>
                            <div className={styles.setting_set} onClick={() => {
                                setIsModalActive(true);
                                setModalType('password');
                            }}>
                                Изменить пароль
                            </div>
                        </div>
                    </div>

                    <div className={styles.exit} onClick={() => logoutUser()}>
                        <img src={exitImg} alt="Exit" />
                        <div>Выйти</div>
                    </div>
                </div>

            </section>

            <NewFooter />
        </>
    )
}


export default UserProfilePage;