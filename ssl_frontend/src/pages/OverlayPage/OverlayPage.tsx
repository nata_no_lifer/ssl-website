import React, {useState} from 'react';
import { Btn, Overlay } from 'shared';



const OverlayPage: React.FC = () => {

    const [isOpen, setIsOpen] = useState(false)

    const handleOpen = () => {
        setIsOpen(true);
    };

    const handleClose = () => {
        setIsOpen(false);
    };

    return (
        <div className="">
            <Btn onClick={handleOpen}>Блюр</Btn>
            <Overlay isActive = {isOpen} onClick={handleClose}/>
        </div>
    )
};

export default OverlayPage;
